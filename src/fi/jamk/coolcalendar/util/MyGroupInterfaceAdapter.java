package fi.jamk.coolcalendar.util;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fi.jamk.coolcalendar.DetailGroupActivity;
import fi.jamk.coolcalendar.R;

public class MyGroupInterfaceAdapter extends ArrayAdapter<String> {
	  private final Context context;
	  private final ArrayList<String> values;
	  private final Activity activity;
	  private final List<String> groupIds;

	  public MyGroupInterfaceAdapter(Context context, ArrayList<String> values, Activity activity, List<String> groupIds) {
	    super(context, R.layout.rowlayout, values);
	    this.context = context;
	    this.values = values;
	    this.activity = activity;
	    this.groupIds = groupIds;
	  }

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.label);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    textView.setText(values.get(position));

	    String s = values.get(position);

	    imageView.setImageResource(android.R.drawable.ic_menu_edit);
	    
	    imageView.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
          	  Intent intent = new Intent(activity, DetailGroupActivity.class);
          	  intent.putExtra("groupId", groupIds.get(position));
          	  activity.startActivityForResult(intent, 1);
	        }
	    });

	    return rowView;
	  }
}