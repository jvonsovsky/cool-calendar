package fi.jamk.coolcalendar.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import fi.jamk.coolcalendar.BaseActivity;
import fi.jamk.coolcalendar.ChatGroupActivity;
import fi.jamk.coolcalendar.CreateNoteActivity;
import fi.jamk.coolcalendar.EventInterfaceActivity;
import fi.jamk.coolcalendar.GroupInterfaceActivity;
import fi.jamk.coolcalendar.R;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.Event;
import fi.jamk.coolcalendar.entity.Group;
import fi.jamk.coolcalendar.entity.Message;
import fi.jamk.coolcalendar.entity.Note;
import fi.jamk.coolcalendar.entity.Settings;
import fi.jamk.coolcalendar.entity.User;

public class MyStorage extends Application {

	private String userId;
    private User user;
    private Settings settings;
    private ArrayList<Course> courses;
    private Map<String, Course> courseMap;

    // null is used as indicator for loading in maps
    // Note is stored by courseId and noteId for easy update and searching by course
    private Map<String, Map<String, Note> > notesMap = null;
    private Map<String, Map<String, Event> > eventsMap = null;
    private Map<String, Group> groupsMap = null;
    private Map<String, Map<String, Message> > messagesMap = null;
    private Map<String, ArrayList<String> > messagesOrderedList = null;
    
    // serves for trasmitting data from findfriend to friendcourse
    private Map<String, ArrayList<Course>> userCoursesMap = null;

	private static Context context;

	public void onCreate() {
        super.onCreate();
        MyStorage.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyStorage.context;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

    public ArrayList<Course> getCourses() {
		return courses;
	}

	public void setCourses(ArrayList<Course> courses) {
		this.courses = courses;
	}

    // course Id, note Id
	public Map<String, Map<String, Note>> getNotesMap() {
		return notesMap;
	}

	public void setNotesMap(Map<String, Map<String, Note>> notesMap) {
		this.notesMap = notesMap;
	}

    // course Id, event Id
	public Map<String, Map<String, Event>> getEventsMap() {
		return eventsMap;
	}

	public void setEventsMap(Map<String, Map<String, Event>> eventsMap) {
		this.eventsMap = eventsMap;
	}

    // course Id, group Id
	public Map<String, Group> getGroupsMap() {
		return groupsMap;
	}

	public void setGroupsMap(Map<String, Group> groupsMap) {
		this.groupsMap = groupsMap;
	}

    // group Id, message Id
	public Map<String, Map<String, Message>> getMessagesMap() {
		return messagesMap;
	}

	public void setMessagesMap(Map<String, Map<String, Message>> messagesMap) {
		this.messagesMap = messagesMap;
	}

	public Map<String, ArrayList<String>> getMessagesOrderedList() {
		return messagesOrderedList;
	}

	public void setMessagesOrderedList(
			Map<String, ArrayList<String>> messagesOrderedList) {
		this.messagesOrderedList = messagesOrderedList;
	}
	
	public Map<String, ArrayList<Course>> getUserCoursesMap() {
		return userCoursesMap;
	}

	public void setUserCoursesMap(Map<String, ArrayList<Course>> userCoursesMap) {
		this.userCoursesMap = userCoursesMap;
	}
	
	public Map<String, Course> getCourseMap() {
		return courseMap;
	}

	public void setCourseMap(Map<String, Course> courseMap) {
		this.courseMap = courseMap;
	}

	/*
	 * Translates user json into user object
	 */
	public void saveUserByJson(JSONObject juser) throws JSONException {
		String id;
		String username = null;
		String password = null;
		String fullname = null;
		String email = null;
		Boolean admin = false;
		
		if (!juser.has("_id"))
			throw new JSONException("User did not return id.");
		
		id = juser.getString("_id");
    	if (juser.has("username"))
    		username = juser.getString("username");
    	if (juser.has("password"))
    		password = juser.getString("password");
    	if (juser.has("fullname"))
    		fullname = juser.getString("fullname");
    	if (juser.has("email"))
    		email = juser.getString("email");
    	if (juser.has("admin"))
    		admin = juser.getBoolean("admin");
    	
    	User user = new User(id, username, password, fullname, email, admin);
    	setUser(user);
    	setUserId(id);
    }
    
	public ArrayList<User> getUsersByJson(JSONArray jusers) throws JSONException {
		String id = null;
		String fullname = null;
		String email = null;
		ArrayList<User> users = new ArrayList<User>();
		
		for (int i = 0; i < jusers.length(); i++) {
			JSONObject jcourse = jusers.getJSONObject(i);
			
			if (jcourse.has("_id"))
				id = jcourse.getString("_id");
			if (jcourse.has("fullname"))
				fullname = jcourse.getString("fullname");
			if (jcourse.has("email"))
				email = jcourse.getString("email");

	    	User user = new User(id, null, null, fullname, email, false);
			users.add(user);
		}
		return users;
	}
	
	public void saveSettingsByJson(JSONObject juser) throws JSONException {
    	String cOn = "on";
    	String cTimer = "15";
    	String theme = "Light";
		
		if (juser.has("countdown_on"))
			cOn = juser.getString("countdown_on");
		if (juser.has("countdown_timer"))
			cTimer = juser.getString("countdown_timer");
		if (juser.has("theme"))
			theme = juser.getString("theme");
    	
		int cIntTimer = 0;
		if (cTimer != null && !cTimer.isEmpty())
			cIntTimer = Integer.parseInt(cTimer);

		Settings settings = new Settings(user, cOn.equals("on"), cIntTimer, theme);
    	setSettings(settings);
    	BaseActivity.saveTheme(theme);
    }
	
	public void saveCoursesByJson(JSONArray jcourses) throws JSONException {
		ArrayList<Course> courses = getCoursesByJson(jcourses, null);
		Map<String, Course> cMap = new HashMap<String, Course>();

		for (Course c : courses) {
			cMap.put(c.getId(), c);
		}
		
		setCourses(courses);
		setCourseMap(cMap);
		saveEventsAndCoursesPreferences();
	}
	
	public ArrayList<Course> getCoursesByJson(JSONArray jcourses, Map<String, User> users) throws JSONException {
		String id = null;
		String name = null;
		String teacher = null;
		String day = null;
		String start = null;
		String end = null;
		String extra_info = null;
		String userId = null;
		String cOn = "on";
		courses = new ArrayList<Course>();
		
		for (int i = 0; i < jcourses.length(); i++) {
			JSONObject jcourse = jcourses.getJSONObject(i);
			
			if (jcourse.has("_id"))
				id = jcourse.getString("_id");
			if (jcourse.has("name"))
				name = jcourse.getString("name");
			if (jcourse.has("teacher"))
				teacher = jcourse.getString("teacher");
			if (jcourse.has("day"))
				day = jcourse.getString("day");
			if (jcourse.has("start"))
				start = jcourse.getString("start");
			if (jcourse.has("end"))
				end = jcourse.getString("end");
			if (jcourse.has("extra_info"))
				extra_info = jcourse.getString("extra_info");
			if (jcourse.has("user"))
				userId = jcourse.getString("user");
			if (jcourse.has("countdown_on"))
				cOn = jcourse.getString("countdown_on");
			Log.v("Course " + i, name);
			
			User sUser = user;
			if (users != null && users.get(userId) != null) {
				sUser = users.get(userId);
			}
			Course course = new Course(id, sUser, name, teacher, day, start, end, extra_info, cOn.equals("on"));
			courses.add(course);
		}
		return courses;
	}

	public void saveNoteByJson(Course course, JSONObject json) throws JSONException {
		String id = "";
		String strDate = null;
		String text = "";
		String user = "";
		String author = "";
		String authorname = "";
		String sharable = "";

		String timeZone = getString(R.string.time_zone);
		Calendar cal = Calendar.getInstance( TimeZone.getTimeZone( timeZone ) );
		
		if (json.has("_id"))
			id = json.getString("_id");
		if (json.has("date")) {
			strDate = json.getString("date");
	    	String[] dataParts = strDate.split("-");
			if (dataParts.length < 3) {
				throw new JSONException("Error in decoding date");
			}
	    	cal = new GregorianCalendar(Integer.valueOf(dataParts[0]), Integer.valueOf(dataParts[1]) - 1, Integer.valueOf(dataParts[2]));
		}
		if (json.has("text"))
			text = json.getString("text");
		if (json.has("user"))
			user = json.getString("user");
		if (json.has("author"))
			author = json.getString("author");
		if (json.has("authorname"))
			authorname = json.getString("authorname");
		if (json.has("sharable"))
			sharable = json.getString("sharable");
    	
    	Log.v("author", cal.getTime().toString());
		Note note = new Note(id, user, author, authorname, course, cal.getTime(), text, sharable.equals("on"));
    	CreateNoteActivity.saveNote(note);
    }

	public void saveEventByJson(User user, Course course, JSONObject json) throws JSONException {
		String id = "";
		String strDate = null;
		String description = "";
		String userId = "";
		String courseId = "";
		String cOn = "on";

		String timeZone = getString(R.string.time_zone);
		Calendar cal = Calendar.getInstance( TimeZone.getTimeZone( timeZone ) );
		
		if (json.has("_id"))
			id = json.getString("_id");
		if (json.has("deadline")) {
			strDate = json.getString("deadline");
	    	String[] dataParts = strDate.split("-");
			if (dataParts.length < 3) {
				throw new JSONException("Error in decoding date");
			}
	    	cal = new GregorianCalendar(Integer.valueOf(dataParts[0]), Integer.valueOf(dataParts[1]) - 1, Integer.valueOf(dataParts[2]));
		}
		if (json.has("description"))
			description = json.getString("description");
		if (json.has("user"))
			userId = json.getString("user");
		if (json.has("course"))
			courseId = json.getString("course");
		if (json.has("countdown_on"))
			cOn = json.getString("countdown_on");
    	
    	Log.v("time", cal.getTime().toString());
		Event event = new Event(id, user, course, description, cal.getTime(), cOn.equals("on"));
    	EventInterfaceActivity.saveEvent(event);
    }

	public void saveGroupByJson(User user, JSONObject json) throws JSONException {
		String id = "";
		String name = "";
		String teacher = "";
		String day = "";
		String start = "";
		String end = "";
		
		if (json.has("_id"))
			id = json.getString("_id");
		if (json.has("name"))
			name = json.getString("name");
		if (json.has("teacher"))
			teacher = json.getString("teacher");
		if (json.has("day"))
			day = json.getString("day");
		if (json.has("start"))
			start = json.getString("start");
		if (json.has("end"))
			start = json.getString("end");
    	
		Group group = new Group(id, user, name, teacher, day, start, end);
    	GroupInterfaceActivity.saveGroup(group);
    }

	public void saveMessageByJson(User user, Group group, JSONObject json) throws JSONException {
		String id = "";
		String timestamp = "";
		String text = "";
		
		if (json.has("_id"))
			id = json.getString("_id");
		if (json.has("timestamp"))
			timestamp = json.getString("timestamp");
		if (json.has("text"))
			text = json.getString("text");
    	
		Message message = new Message(id, group, user, timestamp, text);
    	ChatGroupActivity.saveMessage(message);
    }

	public ArrayList<Message> getMessagesByJson(JSONArray jmessages, Group group, Map<String, User> usersMap) throws JSONException {
		String id = null;
		String juserId = null;
		String timestamp = null;
		String text = null;
		ArrayList<Message> messages = new ArrayList<Message>();
		
		for (int i = 0; i < jmessages.length(); i++) {
			JSONObject jcourse = jmessages.getJSONObject(i);
			
			if (jcourse.has("_id"))
				id = jcourse.getString("_id");
			if (jcourse.has("author"))
				juserId = jcourse.getString("author");
			if (jcourse.has("timestamp"))
				timestamp = jcourse.getString("timestamp");
			if (jcourse.has("text"))
				text = jcourse.getString("text");

	    	User user = usersMap.get(juserId);
	    	Log.v("message", id + ";" + juserId + ";" + timestamp + ";" + text);
			Message message = new Message(id, group, user, timestamp, text);
			messages.add(message);
		}

		return messages;
	}
	
	// saves eventsMap of current user to preferences so it can be used by service for countdown
	public void saveEventsAndCoursesPreferences() {
		if (!settings.getCountdown_on()) {
			return;
		}
		
		Context context = this.getApplicationContext();
		SharedPreferences sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);
		String eventsJson = sharedPref.getString("events", "{ users:  { " + user.getId() + " : { name: \"" + user.getFullname() + "\", timer: 15, events: {}, courses: {} } } }");

		// saves events map to json
		String jsonToSave = eventsJson;
		try {
			JSONObject json = new JSONObject(eventsJson);
			//JSONObject jsonUsers = new JSONObject();
			JSONObject jsonUsers = json.getJSONObject("users");
			JSONObject jsonUserData = jsonUsers.getJSONObject(user.getId());
			jsonUserData.put("name", user.getFullname());
			jsonUserData.put("timer", settings.getCountdown_timer());
			JSONObject jsonEvents = jsonUserData.getJSONObject("events");
		
			if (eventsMap != null) {
				Iterator it = eventsMap.entrySet().iterator();
		        while (it.hasNext()) {
		            Map.Entry pairs = (Map.Entry)it.next();
		            String courseId = ((String)pairs.getKey());
		            Map<String, Event> courseEventMap = (Map<String, Event>)pairs.getValue();
		            
		            Iterator it2 = courseEventMap.entrySet().iterator();
					JSONArray jsonCourseEvents = new JSONArray();
		            while (it2.hasNext()) {
			            Map.Entry pairs2 = (Map.Entry)it2.next();
			            String eventId = ((String)pairs2.getKey());
			            Event event = (Event)pairs2.getValue();

						JSONObject jsonEvent = new JSONObject();
			
						SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						String formatDate = df.format(event.getDeadline());
						
						jsonEvent.put("id", event.getId());
						jsonEvent.put("description", event.getDescription());
						jsonEvent.put("deadline", formatDate);
						jsonEvent.put("countdown", event.getCountdown_on());
						jsonCourseEvents.put(jsonEvent);
			
		            }
					jsonEvents.put(courseId, jsonCourseEvents);
		        }
			}
			jsonUserData.put("events", jsonEvents);
			
			JSONArray jsonCourses = new JSONArray();
			if (courses != null && courses.size() > 0) {
		            for (Course c : courses) {
						JSONObject jsonCourse = new JSONObject();
			
						jsonCourse.put("id", c.getId());
						jsonCourse.put("name", c.getName());
						jsonCourse.put("day", c.getDay());
						jsonCourse.put("start", c.getStart());
						jsonCourse.put("countdown", c.getCountdown_on());
						jsonCourses.put(jsonCourse);
			
		            }
			}

			jsonUserData.put("courses", jsonCourses);
			jsonUsers.put(user.getId(), jsonUserData);
			json.put("users", jsonUsers);
			
			jsonToSave = json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		Log.v("jsonToSave", jsonToSave);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("events", jsonToSave);
		editor.commit();
	}

}