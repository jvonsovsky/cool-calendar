package fi.jamk.coolcalendar.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.R;
import fi.jamk.coolcalendar.entity.Course;

public class MyFriendCoursesAdapter extends ArrayAdapter<String> {
	  private final Context context;
	  private final ArrayList<String> values;
	  private final Activity activity;
	  private final ArrayList<Course> courses;
	  private final String userId;
	  
	  private Map<String, Course> myCourses;
	  private Map<String, Course> myCoursesByNames;

	  public MyFriendCoursesAdapter(Context context, ArrayList<String> values, Activity activity, ArrayList<Course> courses, String userId) {
	    super(context, R.layout.rowlayout, values);
	    this.context = context;
	    this.values = values;
	    this.activity = activity;
	    this.courses = courses;
	    this.userId = userId;

	     myCourses = ((MyStorage)MyStorage.getAppContext()).getCourseMap();
	     setMyCoursesByNames();
	  }
	  
	  private void setMyCoursesByNames() {
		  myCoursesByNames = new HashMap<String, Course>();
		  
		  if (myCourses == null)
			  return;
		  
		  	Iterator it = myCourses.entrySet().iterator();
			while (it.hasNext()) {
			    Map.Entry pairs = (Map.Entry)it.next();
			    Course c = (Course)pairs.getValue();
			    myCoursesByNames.put(c.getName(), c);
			}
	  }
	  
	  private void showAlreadyIn(final String courseId) {
		    new AlertDialog.Builder(context)
	        .setIcon(android.R.drawable.ic_dialog_info)
	        .setTitle("Add notes")
	        .setMessage("You are already in this course. Do you want to copy the notes?")
	        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
		    {
		        @Override
		        public void onClick(DialogInterface dialog, int which) {
		        	String retString =
		    		    	DBService.copyNotes(userId, courseId);
	    	    	if (retString != "") {
	    	    		Toast.makeText(context, retString, Toast.LENGTH_LONG).show();
	    	    	} else {
	    	    		Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
	    	    	}
	    	    	activity.finish();
		        }
		
		    })
		    .setNegativeButton("No", null)
		    .show();
	  }
	  
	  private void showConfirmToAdd(final String courseId) {
		    new AlertDialog.Builder(context)
	        .setIcon(android.R.drawable.ic_dialog_info)
	        .setTitle("Add course")
	        .setMessage("Are you sure you want to enter to this course?")
	        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
		    {
		        @Override
		        public void onClick(DialogInterface dialog, int which) {
		        	String retString =
		    		    	DBService.copyCourses(userId, courseId);
	    	    	if (retString != "") {
	    	    		Toast.makeText(context, retString, Toast.LENGTH_LONG).show();
	    	    	} else {
	    	    		Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
	    	    	}
		        }
		
		    })
		    .setNegativeButton("No", null)
		    .show();
	  }

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.label);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    textView.setText(values.get(position));
	    // Change the icon for Windows and iPhone
	    String s = values.get(position);

	    Course course = courses.get(position);
	    final Boolean isMember = myCoursesByNames.containsKey(course.getName());
	    final String courseId = course.getId();

	    if (isMember) {
	    	imageView.setImageResource(android.R.drawable.checkbox_on_background);
	    } else {
	    	imageView.setImageResource(android.R.drawable.ic_input_add);
	    }
	    
	    rowView.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
	        	if (isMember) {
	        		showAlreadyIn(courseId);
	        	} else {
	        		showConfirmToAdd(courseId);
	        	}
	        }
	    });
	    
	    imageView.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
	        	if (isMember) {
	        		showAlreadyIn(courseId);
	        	} else {
	        		showConfirmToAdd(courseId);
	        	}
	        }
	    });

	    return rowView;
	  }
}
