package fi.jamk.coolcalendar.util;

import java.util.ArrayList;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fi.jamk.coolcalendar.DetailCourseActivity;
import fi.jamk.coolcalendar.R;
import fi.jamk.coolcalendar.entity.Course;

public class MyCourseAdapter extends ArrayAdapter<String> {
	  private final Activity activity;  
	  private final Context context;
	  private final ArrayList<String> values;
	  private final ArrayList<String> idValues;
	  private final Map<String, Course> courseMap;

	  public MyCourseAdapter(Activity activity, Context context, ArrayList<String> values, ArrayList<String> idValues, Map<String, Course> courseMap) {
	    super(context, R.layout.rowlayout, values);
	    this.activity = activity;
	    this.context = context;
	    this.values = values;
	    this.idValues = idValues;
	    this.courseMap = courseMap;
	  }

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);

	    TextView textView = (TextView) rowView.findViewById(R.id.label);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    //ImageView imageViewExtra = (ImageView) rowView.findViewById(R.id.icon);

	    textView.setText(values.get(position));
	    // Change the icon for Windows and iPhone
	    String s = values.get(position);

	    imageView.setImageResource(android.R.drawable.ic_menu_edit);
	    //imageViewExtra.setImageResource(android.R.drawable.ic_menu_delete);
	    
	    imageView.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
	    		Intent intent = new Intent(activity, DetailCourseActivity.class);
	    		Log.v("kurva", values.get(position));
	    		intent.putExtra("courseId", courseMap.get( idValues.get(position) ).getId());
	    		activity.startActivityForResult(intent, 1);
	        }
	    });
	    
	    /*
	    imageViewExtra.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
			    final String courseId = courseMap.get(values.get(position)).getId();
	        	
	        	new AlertDialog.Builder(context)
		        .setIcon(android.R.drawable.ic_dialog_info)
		        .setTitle("Delete course")
		        .setMessage("Are you sure you want to delete this course?")
		        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
			    {
			        @Override
			        public void onClick(DialogInterface dialog, int which) {
			        	String retString =
			    		    	DBService.deleteCourse(courseId);
		    	    	if (retString != "") {
		    	    		Toast.makeText(context, retString, Toast.LENGTH_LONG).show();
		    	    	} else {
		    	    		Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
		    	    	}

			        }
			
			    })
			    .setNegativeButton("No", null)
			    .show();
	        }
	    });
	    */

	    return rowView;
	  }
}
