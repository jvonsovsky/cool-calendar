package fi.jamk.coolcalendar.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.R;
import fi.jamk.coolcalendar.entity.Group;

public class MyGroupSearchResultAdapter extends ArrayAdapter<String> {
	  private final Context context;
	  private final ArrayList<String> values;
	  private final Activity activity;
	  private final List<Group> groups;
	  
	  private Map<String, Group> myGroups;

	  public MyGroupSearchResultAdapter(Context context, ArrayList<String> values, Activity activity, List<Group> groups) {
	    super(context, R.layout.rowlayout, values);
	    this.context = context;
	    this.values = values;
	    this.activity = activity;
	    this.groups = groups;

	     myGroups = ((MyStorage)MyStorage.getAppContext()).getGroupsMap();
	  }
	  
	  private void showAlreadyIn() {
		  AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);

		  dlgAlert.setMessage("You are already in this group.");
		  dlgAlert.setTitle("Info");
		  dlgAlert.setCancelable(true);
		  dlgAlert.create().show();
		  
		  dlgAlert.setPositiveButton("OK",
			    new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) {
			          // it is dismissed now
			        }
			    });
	  }
	  
	  private void showConfirmToAdd(final String groupId) {
		    new AlertDialog.Builder(context)
	        .setIcon(android.R.drawable.ic_dialog_info)
	        .setTitle("Add course")
	        .setMessage("Are you sure you want to enter to this group?")
	        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
		    {
		        @Override
		        public void onClick(DialogInterface dialog, int which) {
		    		String userId = ((MyStorage)MyStorage.getAppContext()).getUserId();
		        	
		        	String retString =
		    		    	DBService.enterGroup(userId, groupId);
	    	    	if (retString != "") {
	    	    		Toast.makeText(context, retString, Toast.LENGTH_LONG).show();
	    	    	} else {
	    	    		Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
	    	    	}

		        }
		
		    })
		    .setNegativeButton("No", null)
		    .show();
	  }

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.label);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    textView.setText(values.get(position));
	    // Change the icon for Windows and iPhone
	    String s = values.get(position);

	    Group group = groups.get(position);
	    final Boolean isMember = myGroups.containsKey(group.getId());
	    final String groupId = group.getId();

	    if (isMember) {
	    	imageView.setImageResource(android.R.drawable.checkbox_on_background);
	    } else {
	    	imageView.setImageResource(android.R.drawable.ic_input_add);
	    }
	    
	    rowView.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
	        	if (isMember) {
	        		showAlreadyIn();
	        	} else {
	        		showConfirmToAdd(groupId);
	        	}
	        }
	    });
	    
	    imageView.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
	        	if (isMember) {
	        		showAlreadyIn();
	        	} else {
	        		showConfirmToAdd(groupId);
	        	}
	        }
	    });

	    return rowView;
	  }
}