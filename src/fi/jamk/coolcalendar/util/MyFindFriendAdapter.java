package fi.jamk.coolcalendar.util;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fi.jamk.coolcalendar.FindFriendActivity;
import fi.jamk.coolcalendar.R;

public class MyFindFriendAdapter extends ArrayAdapter<String> {
	  private final Context context;
	  private final ArrayList<String> values;
	  private final Activity activity;
	  private final ArrayList<String> userIds;

	  public MyFindFriendAdapter(Context context, ArrayList<String> values, Activity activity, ArrayList<String> userIds) {
	    super(context, R.layout.rowlayout, values);
	    this.context = context;
	    this.values = values;
	    this.activity = activity;
	    this.userIds = userIds;
	  }

	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.label);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    textView.setText(values.get(position));

	    String s = values.get(position);

	    imageView.setImageResource(android.R.drawable.ic_menu_info_details);
	    
	    imageView.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
	        	//((FindFriendActivity)context).showDialog(userIds.get(position));
	        	FindFriendActivity.showDialog(userIds.get(position));
	        }
	    });

	    return rowView;
	  }
}
