package fi.jamk.coolcalendar.util;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.R;

public class MyNoteInterfaceAdapter extends ArrayAdapter<String> {
	  private final Context context;
	  private final ArrayList<String> values;
	  private final Activity activity;
	  private final List<String> notes;

	  public MyNoteInterfaceAdapter(Context context, ArrayList<String> values, Activity activity, List<String> notes) {
	    super(context, R.layout.rowlayout, values);
	    this.context = context;
	    this.values = values;
	    this.activity = activity;
	    this.notes = notes;
	  }

	  private void showConfirmToDelete(final String noteId) {
	    final String userId = ((MyStorage)MyStorage.getAppContext()).getUserId();
  	
	  	new AlertDialog.Builder(context)
	      .setIcon(android.R.drawable.ic_dialog_alert)
	      .setTitle("Delete note")
	      .setMessage("Are you sure you want to delete this note?")
	      .setPositiveButton("Yes", new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	    		String retString =
	    		    	DBService.deleteNote(noteId);
  	    	if (retString != "") {
  	    		Toast.makeText(context, retString, Toast.LENGTH_LONG).show();
  	    	} else {
  	    		Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
  	    	}

	        }
	
	    })
	    .setNegativeButton("No", null)
	    .show();
	  }
	  
	  @Override
	  public View getView(final int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	    	View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
		    TextView textView = (TextView) rowView.findViewById(R.id.label);
		    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		    textView.setText(values.get(position));
	
		    String s = values.get(position);
	
		    final String noteId = notes.get(position);
	
		    imageView.setImageResource(android.R.drawable.ic_menu_delete);
		    imageView.setOnClickListener(new View.OnClickListener() {

	        @Override
	        public void onClick(View v) {
	        	showConfirmToDelete(noteId);
	        }
	    });

	    return rowView;
	  }
}