package fi.jamk.coolcalendar;

import java.util.Map;

import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class DetailCourseActivity extends BaseActivity {

	private String courseId = null;
	private Course course = null;
	
	private EditText fName;
	private EditText fTeacher;
	private Spinner fDay;
	private EditText fExtraInfo;
	private TimePicker tStart;
	private TimePicker tEnd;
	private CheckBox fCOn;
	public static Button fSubmit;
	public static Button fDelete;
	
	public static Context context;
	public static Activity activity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_updatecourse);

		Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			courseId = extras.getString("courseId");
			course = courseMap.get(courseId);
		}
		Log.v("course 1", courseId);
		Log.v("course 2", course.getName());
		
		// this shouldn't happen
		if (course == null) {
			finish();
		}

		context = this;
		activity = this;
		initFields();
		initCallbacks();
		initValues();
		
		fSubmit.setVisibility(View.VISIBLE);
	}

	public void onClickDeleteButton(View view) {
	    new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Delete course")
        .setMessage("Are you sure you want to delete this course?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	    		String retString =
	    		    	DBService.deleteCourse(course.getId());
    	    	if (retString != "") {
    	    		Toast.makeText(context, retString, Toast.LENGTH_LONG).show();
    	    	} else {
    	    		Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
    	    	}

	        }
	
	    })
	    .setNegativeButton("No", null)
	    .show();
	}
	
    private void initFields() {
    	fName = (EditText) findViewById(R.id.fName);
    	fDay = (Spinner) findViewById(R.id.fDay);
    	fExtraInfo = (EditText) findViewById(R.id.fExtraInfo);
    	tStart = (TimePicker) findViewById(R.id.tStart);
    	tEnd = (TimePicker) findViewById(R.id.tEnd);
    	fTeacher = (EditText) findViewById(R.id.fTeacher);
    	fCOn = (CheckBox) findViewById(R.id.fCOn);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    	fDelete = (Button) findViewById(R.id.fDelete);

    	// create days spinner
    	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
    	        R.array.days_array, android.R.layout.simple_spinner_item);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	fDay.setAdapter(adapter);    

    	if (course.getCountdown_on() != null)
    		fCOn.setChecked( course.getCountdown_on() );
    	else fCOn.setChecked( true );
    }
    
    private void initCallbacks() {
        fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    private int getDaySelection(String dayStr) {
    	Resources res = getResources();
    	String[] days = res.getStringArray(R.array.days_array);
    	
    	int i = 0;
    	for (String day : days) {
    		if (day.equals(dayStr)) {
    			return i;
    		}
    		i++;
    	}
    	
    	return 0;
    }
    
    private void initValues() {
    	String strStart = course.getStart();
    	String[] parts = strStart.split(":");
    	if (parts.length >= 2) {
    		tStart.setCurrentHour(Integer.valueOf(parts[0]));
    		tStart.setCurrentMinute(Integer.valueOf(parts[1]));
    	}
    	
    	String strEnd = course.getEnd();
    	parts = strEnd.split(":");
    	if (parts.length >= 2) {
    		tEnd.setCurrentHour(Integer.valueOf(parts[0]));
    		tEnd.setCurrentMinute(Integer.valueOf(parts[1]));
    	}

    	int position = getDaySelection(course.getDay());
    	
    	fName.setText(course.getName());
    	fDay.setSelection(position);
    	fExtraInfo.setText(course.getExtra_info());
    	fTeacher.setText(course.getTeacher());
    }

    private boolean validateFields() {
    	//getString(R.string.username_is_required)
    	
    	boolean valid = true;
    	
    	if (fName.getText().toString().length() == 0) {
    		fName.setError("Name is required.");
    		valid = false;
    	}    	

    	return valid;
    }
    
    private void submit() {
    	if (validateFields()) {
    		String userId = ((MyStorage)MyStorage.getAppContext()).getUserId();
    		fSubmit.setEnabled(false);
    		fDelete.setEnabled(false);
    		String retString =
		    	DBService.updateCourse(course.getId(), userId, fName.getText().toString(), fTeacher.getText().toString(), fDay.getSelectedItem().toString(),
		    			fExtraInfo.getText().toString(), tStart.getCurrentHour() + ":" + tStart.getCurrentMinute(),
		    			tEnd.getCurrentHour() + ":" + tEnd.getCurrentMinute(), fCOn.isChecked() ? "on" : "off");
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	}
    }
    
    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
		fDelete.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				CreateCourseActivity.saveData(json_encoded);
				Toast.makeText(context, "Course updated.", Toast.LENGTH_LONG).show();
				activity.setResult(RESULT_OK, null);
	    		activity.finish();			
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

    public static void deletedCallback(String notify) {
		fSubmit.setEnabled(true);
		fDelete.setEnabled(true);
		if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {
			String json_encoded = notify.substring(8);
			
			try {
				CreateCourseActivity.saveData(json_encoded);
				Toast.makeText(context, "Course deleted.", Toast.LENGTH_LONG).show();
				activity.setResult(RESULT_OK, null);
	    		activity.finish();
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
