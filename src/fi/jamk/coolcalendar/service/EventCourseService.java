package fi.jamk.coolcalendar.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fi.jamk.coolcalendar.CourseFromServiceActivity;
import fi.jamk.coolcalendar.EventFromServiceActivity;
import fi.jamk.coolcalendar.R;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;

public class EventCourseService extends Service {

	private Looper mEventLooper;
	private EventHandler mEventHandler;
	private Resources res;
	private String[] dayNames;
	private Map<String, Integer> dayNamesMap;
	private String timeZone;

	// Handler that receives messages from the thread
	private final class EventHandler extends Handler {
		public EventHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			
			res = getResources();
			dayNames = res.getStringArray(R.array.days_array);
			timeZone = getString(R.string.time_zone);

			// convert names of days to map
			int i = 2;
			dayNamesMap = new HashMap<String, Integer>();
			for (String dayName : dayNames) {
				dayNamesMap.put(dayName, i);
				i++;
			}
			
			// Calendar class has many weird things, cannot set monday as first day
			dayNamesMap.put("Sunday", 1);
			
			while (true) {
				Calendar c = Calendar.getInstance( TimeZone.getTimeZone( timeZone ) );

				if (c.get(Calendar.HOUR_OF_DAY) == 12 && c.get(Calendar.MINUTE) == 0) {
					loadEventsAndCoursesAndCheck(true);
				}
				
				loadEventsAndCoursesAndCheck(false);
	
				// check every minute
				long endTime = System.currentTimeMillis() + 60 * 1000;
				while (System.currentTimeMillis() < endTime) {
					synchronized (this) {
						try {
							wait(endTime - System.currentTimeMillis());
						} catch (Exception e) {
						}
					}
				}
			}

			// Stop the service using the startId, so that we don't stop
			// the service in the middle of handling another job
			//stopSelf(msg.arg1);
		}
	}
	
	private long daysDiffFromNow(String sqlDate) throws ParseException {
		Date d1 = new SimpleDateFormat("yyyy-MM-dd").parse(sqlDate);
		Calendar now = Calendar.getInstance( TimeZone.getTimeZone( timeZone ) );
		
		long diff = d1.getTime() - now.getTime().getTime();
		return diff / (24 * 60 * 60 * 1000) + 1;
	}
	
	private int minutesDiffFromNow(String day, String time) throws ParseException {
		Calendar now = Calendar.getInstance( TimeZone.getTimeZone( timeZone ) );
		
		int diffDays = dayNamesMap.get(day) - now.get(Calendar.DAY_OF_WEEK);
		
		int nowTime = now.get(Calendar.HOUR_OF_DAY) * 60 + now.get(Calendar.MINUTE);
		String[] timeParts = time.split(":");
		int startTime = Integer.valueOf(timeParts[0]) * 60 + Integer.valueOf(timeParts[1]);
		int diffTime = startTime - nowTime;
		
		// convert from negative number
		int diffMinutes = diffDays * 1440 + diffTime;
		while (diffMinutes < 0)
			diffMinutes += 1440 * 7;

		return diffMinutes;
	}
	
	// extracts all events of all users in storage and check if there is something to tell
	private void loadEventsAndCoursesAndCheck(Boolean events) {
		try {
			Context context = this.getApplicationContext();
			SharedPreferences sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);
			//String eventsJson = sharedPref.getString("events", "{ users:  { name: \"" + user.getFullname() + "\", " + user.getId() + " : {} } }");
			String eventsJson = sharedPref.getString("events", "{ users:  { } }");
			
			Log.v("eventsjson", eventsJson);
			
			JSONObject json = new JSONObject(eventsJson);
			JSONObject jsonUsers = json.getJSONObject("users");
			Iterator<String> iter = jsonUsers.keys();
		    while (iter.hasNext()) {
		        String userId = iter.next();
		        JSONObject jsonUserData = jsonUsers.getJSONObject(userId);
		        Log.v("json userId", userId);
		        
		        String fullname = jsonUserData.getString("name");
		        int timer = jsonUserData.getInt("timer");
		        JSONObject jsonEvents = jsonUserData.getJSONObject("events");
		        JSONArray jsonCourses = jsonUserData.getJSONArray("courses");
		        
		        if (jsonEvents != null && events) {
		        	Iterator<String> iterEvent = jsonEvents.keys();
		        	while (iterEvent.hasNext()) {
		        		String courseId = iterEvent.next();
		        		JSONArray jEvents = jsonEvents.getJSONArray(courseId);
		        		
		        		for (int i = 0; i < jEvents.length(); i++) {
		        			JSONObject jEvent = jEvents.getJSONObject(i);

		        			String eventId = jEvent.getString("id");
		        			String eventDesc = jEvent.getString("description");
		        			String deadline = jEvent.getString("deadline");
		        			Boolean countdown = jEvent.getBoolean("countdown");

		        			Log.v("event data", userId + ";" + fullname + ";" + courseId + ";" + eventId + ";" + eventDesc + ";" + deadline + ";" + countdown);
		        			long diffDays = daysDiffFromNow(deadline);
		        			Log.v("diff days", String.valueOf(diffDays));
		        			
		        			// let's show alert
		        			if (countdown && diffDays <= 10 && diffDays > 0) {
		        				Intent dialogIntent = new Intent(getBaseContext(), EventFromServiceActivity.class);
		    					dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    					dialogIntent.putExtra("userId", userId);
		    					dialogIntent.putExtra("fullname", fullname);
		    					dialogIntent.putExtra("courseId", courseId);
		    					dialogIntent.putExtra("eventId", eventId);
		    					dialogIntent.putExtra("description", eventDesc);
		    					dialogIntent.putExtra("deadline", deadline);
		    					dialogIntent.putExtra("diff", diffDays);
		    					getApplication().startActivity(dialogIntent);
		        			}
		        		}
		        	}
		        }
		        
		        if (jsonCourses != null && !events) {
	        		for (int i = 0; i < jsonCourses.length(); i++) {
	        			JSONObject jCourse = jsonCourses.getJSONObject(i);

	        			String courseId = jCourse.getString("id");
	        			String name = jCourse.getString("name");
	        			String day = jCourse.getString("day");
	        			String start = jCourse.getString("start");
	        			Boolean countdown = true;
	        			if (jCourse.has("countdown"))
	        				countdown = jCourse.getBoolean("countdown");

	        			//Log.v("event data", userId + ";" + fullname + ";" + courseId + ";" + eventId + ";" + eventDesc + ";" + deadline + ";" + countdown);
	        			int diffMinutes = minutesDiffFromNow(day, start);
	        			Log.v("day", day);
	        			Log.v("start", start);
	        			Log.v("diff minutes", String.valueOf(diffMinutes));
	        			//Log.v("diff days", String.valueOf(diffDays));
	        			
	        			// let's show alert
	        			if (countdown && diffMinutes == timer) {
	        				Intent dialogIntent = new Intent(getBaseContext(), CourseFromServiceActivity.class);
	    					dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    					dialogIntent.putExtra("userId", userId);
	    					dialogIntent.putExtra("fullname", fullname);
	    					dialogIntent.putExtra("courseId", courseId);
	    					dialogIntent.putExtra("name", name);
	    					dialogIntent.putExtra("day", day);
	    					dialogIntent.putExtra("start", start);
	    					dialogIntent.putExtra("diff", diffMinutes);
	    					getApplication().startActivity(dialogIntent);
	        			}
	        		}
	        	}
		        
		    }
		} catch (JSONException e) {
			Log.v("json exception", e.getMessage());
		} catch (ParseException e) {
			Log.v("parse exception", e.getMessage());
		}
	}

	@Override
	public void onCreate() {
		//Toast.makeText(this, "service createde", Toast.LENGTH_SHORT).show();
		HandlerThread thread = new HandlerThread("ServiceStartArguments",
				Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();

		// Get the HandlerThread's Looper and use it for our Handler
		mEventLooper = thread.getLooper();
		mEventHandler = new EventHandler(mEventLooper);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the job
		Message msg = mEventHandler.obtainMessage();
		msg.arg1 = startId;
		mEventHandler.sendMessage(msg);

		// If we get killed, after returning from here, restart
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onDestroy() {
		//Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
	}

}
