package fi.jamk.coolcalendar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.Note;
import fi.jamk.coolcalendar.util.MyNoteInterfaceAdapter;
import fi.jamk.coolcalendar.util.MyStorage;

public class NoteInterfaceActivity extends BaseActivity {
		
	private String courseId;
	private static Course course = null;
	private TextView tUserMsg;
	private ListView listView;
	
	public static Context context;
	public static Activity activity;

	// ids from notes map will be stored here so we can get id by listview position
	private List<String> noteIds;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_noteinterface);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
		// get course
    	Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			courseId = extras.getString("courseId");
			course = courseMap.get(courseId);
		}
		
		// this shouldn't happen
		if (course == null) {
			finish();
		}

    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	listView = (ListView) findViewById(R.id.noteList);
    	
    	tUserMsg.setText("Review of " + course.getName());
    	
    	// get notes
    	noteIds = new ArrayList<String>();
    	ArrayList<String> values = new ArrayList<String>();
    	Map<String, Map<String, Note>> notes = ((MyStorage)MyStorage.getAppContext()).getNotesMap();
    	SimpleDateFormat df= new SimpleDateFormat("yyyy-MM-dd");
    	if (notes != null) {
	    	Map<String, Note> courseNotes = notes.get(course.getId());
	    	
	    	Iterator it = courseNotes.entrySet().iterator();
	        while (it.hasNext()) {
	            Map.Entry pairs = (Map.Entry)it.next();
	            noteIds.add((String)pairs.getKey());
	            Note note = (Note)pairs.getValue();

			    df.applyPattern("dd.MM.yyyy");
			    String newDate = df.format(note.getDate());  //Output: newDate = "13/09/2014"

	            values.add(newDate);
	        }
    	}
    	
    	MyNoteInterfaceAdapter adapter = new MyNoteInterfaceAdapter(context, values, activity, noteIds);
   		listView.setAdapter(adapter);
    }
    
    private void initCallbacks() {

    	
    	// ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {

              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) {
                
            	  Intent intent = new Intent(NoteInterfaceActivity.this, DetailNoteActivity.class);
            	  intent.putExtra("courseId", courseId);
            	  intent.putExtra("noteId", noteIds.get(position));
            	  startActivityForResult(intent, 1);
              }

         });
    }

    public static void deleteFromNote(String noteId) {
    	Map<String, Map<String, Note>> notes = ((MyStorage)MyStorage.getAppContext()).getNotesMap();
    	
    	if (notes != null) {
    		Map<String, Note> courseNotes = notes.get(course.getId());
    		if (courseNotes != null) {
    			courseNotes.remove(noteId);
    		}
    		notes.put(course.getId(), courseNotes);
    	}
    	
    	((MyStorage)MyStorage.getAppContext()).setNotesMap(notes);
    }
    
    public static void deletedCallback(String notify) {
		if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String noteId = notify.substring(8).trim();
			
			activity.finish();
			activity.startActivity(activity.getIntent());
			deleteFromNote(noteId);
			Toast.makeText(context, "Note deleted.", Toast.LENGTH_LONG).show();
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      if(resultCode == RESULT_OK){
    	  initFields();
    	  initCallbacks();
      }
	}

}
