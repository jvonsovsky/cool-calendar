package fi.jamk.coolcalendar;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.Event;
import fi.jamk.coolcalendar.entity.Group;
import fi.jamk.coolcalendar.entity.Note;
import fi.jamk.coolcalendar.entity.Settings;
import fi.jamk.coolcalendar.entity.User;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.EditText;
import android.widget.TimePicker;

public class MyStorage extends Application {

    private String userId;
    private User user;
    private Settings settings;
    private ArrayList<Course> courses;
    // Note is stored by courseId and noteId for easy update and searching by course
    private Map<String, Map<String, Note> > notesMap;
    private Map<String, Map<String, Event> > eventsMap;
    private Map<String, Group> groupsMap;
    // groups of current user only
    private Map<String, Group> userGroupsMap;

	private static Context context;

	public void onCreate(){
        super.onCreate();
        MyStorage.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyStorage.context;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(Settings settings) {
		this.settings = settings;
	}

    public ArrayList<Course> getCourses() {
		return courses;
	}

	public void setCourses(ArrayList<Course> courses) {
		this.courses = courses;
	}

    // course Id, note Id
	public Map<String, Map<String, Note>> getNotesMap() {
		return notesMap;
	}

	public void setNotesMap(Map<String, Map<String, Note>> notesMap) {
		this.notesMap = notesMap;
	}

    // course Id, event Id
	public Map<String, Map<String, Event>> getEventsMap() {
		return eventsMap;
	}

	public void setEventsMap(Map<String, Map<String, Event>> eventsMap) {
		this.eventsMap = eventsMap;
	}

    // course Id, group Id
	public Map<String, Group> getGroupsMap() {
		return groupsMap;
	}

	public void setGroupsMap(Map<String, Group> groupsMap) {
		this.groupsMap = groupsMap;
	}

    // course Id, group Id
	// groups of current user only
	public Map<String, Group> getUserGroupsMap() {
		return userGroupsMap;
	}

	public void setUserGroupsMap(Map<String, Group> userGroupsMap) {
		this.userGroupsMap = userGroupsMap;
	}

	/*
	 * Translates user json into user object
	 */
	public void saveUserByJson(JSONObject juser) throws JSONException {
		String id;
		String username = null;
		String password = null;
		String fullname = null;
		String email = null;
		Boolean admin = false;
		
		if (!juser.has("_id"))
			throw new JSONException("User did not return id.");
		
		id = juser.getString("_id");
    	if (juser.has("username"))
    		username = juser.getString("username");
    	if (juser.has("password"))
    		password = juser.getString("password");
    	if (juser.has("fullname"))
    		fullname = juser.getString("fullname");
    	if (juser.has("email"))
    		email = juser.getString("email");
    	if (juser.has("admin"))
    		admin = juser.getBoolean("admin");
    	
    	User user = new User(id, username, password, fullname, email, admin);
    	setUser(user);
    	setUserId(id);
    }
    
	public void saveSettingsByJson(JSONObject juser) throws JSONException {
    	String cOn = "on";
    	String cTimer = "15";
    	String color = "white";
		
		if (juser.has("countdown_on"))
			cOn = juser.getString("countdown_on");
		if (juser.has("countdown_timer"))
			cTimer = juser.getString("countdown_timer");
		if (juser.has("color"))
			color = juser.getString("color");
    	
		int cIntTimer = 0;
		if (cTimer != null && !cTimer.isEmpty())
			cIntTimer = Integer.parseInt(cTimer);
		Log.v("con", cOn);
		Settings settings = new Settings(user, cOn.equals("on"), cIntTimer, color);
		Log.v("settings save", settings.toString());
    	setSettings(settings);
    }
	
	public void saveCoursesByJson(JSONArray jcourses) throws JSONException {
		String id = null;
		String name = null;
		String teacher = null;
		String day = null;
		String start = null;
		String end = null;
		String extra_info = null;
		courses = new ArrayList<Course>();
		
		for (int i = 0; i < jcourses.length(); i++) {
			JSONObject jcourse = jcourses.getJSONObject(i);
			
			if (jcourse.has("_id"))
				id = jcourse.getString("_id");
			if (jcourse.has("name"))
				name = jcourse.getString("name");
			if (jcourse.has("teacher"))
				teacher = jcourse.getString("teacher");
			if (jcourse.has("day"))
				day = jcourse.getString("day");
			if (jcourse.has("start"))
				start = jcourse.getString("start");
			if (jcourse.has("end"))
				end = jcourse.getString("end");
			if (jcourse.has("extra_info"))
				extra_info = jcourse.getString("extra_info");
			Log.v("Course " + i, name);
			
			Course course = new Course(id, user, name, teacher, day, start, end, extra_info);
			courses.add(course);
		}
		setCourses(courses);
	}

	public void saveNoteByJson(Course course, JSONObject json) throws JSONException {
		String id = "";
		String strDate = null;
		String text = "";
		String author = "";
		Calendar cal = Calendar.getInstance();
		
		if (json.has("_id"))
			id = json.getString("_id");
		if (json.has("date")) {
			strDate = json.getString("date");
	    	String[] dataParts = strDate.split("-");
			if (dataParts.length < 3) {
				throw new JSONException("Error in decoding date");
			}
	    	cal = new GregorianCalendar(Integer.valueOf(dataParts[0]), Integer.valueOf(dataParts[1]) - 1, Integer.valueOf(dataParts[2]));
		}
		if (json.has("text"))
			text = json.getString("text");
		if (json.has("author"))
			author = json.getString("author");
    	
    	Log.v("author", cal.getTime().toString());
		Note note = new Note(id, author, course, cal.getTime(), text);
    	CreateNoteActivity.saveNote(note);
    }

	public void saveEventByJson(User user, Course course, JSONObject json) throws JSONException {
		String id = "";
		String strDate = null;
		String description = "";
		String userId = "";
		String courseId = "";
		String cOn = "on";
		Calendar cal = Calendar.getInstance();
		
		if (json.has("_id"))
			id = json.getString("_id");
		if (json.has("deadline")) {
			strDate = json.getString("deadline");
	    	String[] dataParts = strDate.split("-");
			if (dataParts.length < 3) {
				throw new JSONException("Error in decoding date");
			}
	    	cal = new GregorianCalendar(Integer.valueOf(dataParts[0]), Integer.valueOf(dataParts[1]) - 1, Integer.valueOf(dataParts[2]));
		}
		if (json.has("description"))
			description = json.getString("description");
		if (json.has("user"))
			userId = json.getString("user");
		if (json.has("course"))
			courseId = json.getString("course");
		if (json.has("countdown_on"))
			cOn = json.getString("countdown_on");
    	
    	Log.v("time", cal.getTime().toString());
		Event event = new Event(id, user, course, description, cal.getTime(), cOn.equals("on"));
    	EventInterfaceActivity.saveEvent(event);
    }

	public void saveGroupByJson(User user, JSONObject json) throws JSONException {
		String id = "";
		String name = "";
		String teacher = "";
		String day = "";
		String start = "";
		String end = "";
		
		if (json.has("_id"))
			id = json.getString("_id");
		if (json.has("name"))
			name = json.getString("name");
		if (json.has("teacher"))
			teacher = json.getString("teacher");
		if (json.has("day"))
			day = json.getString("day");
		if (json.has("start"))
			start = json.getString("start");
		if (json.has("end"))
			start = json.getString("end");
    	
		Group group = new Group(id, user, name, teacher, day, start, end);
    	GroupInterfaceActivity.saveGroup(group);
    }

}