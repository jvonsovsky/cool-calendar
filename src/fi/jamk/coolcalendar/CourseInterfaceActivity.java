package fi.jamk.coolcalendar;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class CourseInterfaceActivity extends BaseActivity {

	public static String courseId;
	public static Course course = null;
	private TextView tUserMsg;

	public static Activity activity;
	public static Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_courseinterface);

		initFields();
	}


    private void initFields() {
    	Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			courseId = extras.getString("courseId");
			course = courseMap.get(courseId);
		}

		// this shouldn't happen
		if (course == null) {
			finish();
		}
		
		activity = this;
		context = this;

    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	
    	tUserMsg.setText("Course " + course.getName());
    }
    
    public void onClickDetailCourseButton(View view) {
		Intent intent = new Intent(CourseInterfaceActivity.this, DetailCourseActivity.class);
		intent.putExtra("courseId", courseId);
		startActivityForResult(intent, 1);
    }
    
    public void onClickMakeNotesButton(View view) {
		Intent intent = new Intent(CourseInterfaceActivity.this, CreateNoteActivity.class);
		intent.putExtra("courseId", courseId);
		startActivity(intent);
    }

    public void onClickReviewButton(View view) {
    	// if data for this course weren't loaded yet
    	if (((MyStorage)MyStorage.getAppContext()).getNotesMap() == null ||
    			((MyStorage)MyStorage.getAppContext()).getNotesMap().get(course.getId()) == null) {
	    	String retString = "";
	
			retString =	DBService.getNotes(course.getId());
	
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	} else {
    		// no need for grabbing data again
			Intent intent = new Intent(activity, NoteInterfaceActivity.class);
			intent.putExtra("courseId", courseId);
			activity.startActivity(intent);
    	}

    }
    
    public void onClickEventsButton(View view) {
    	// if data for this course weren't loaded yet
    	//if (((MyStorage)MyStorage.getAppContext()).getEventsMap() == null ||
    	//		((MyStorage)MyStorage.getAppContext()).getEventsMap().get(course.getId()) == null) {
	    	User user = ((MyStorage)MyStorage.getAppContext()).getUser();
	    	String retString = "";
	
			retString =	DBService.getEvents(user.getId(), course.getId());
	
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	/*} else {
    		// no need for grabbing data again
			Intent intent = new Intent(activity, EventInterfaceActivity.class);
			intent.putExtra("courseId", courseId);
			activity.startActivity(intent);
    	}*/
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      if(resultCode == RESULT_OK) {
          Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();

          // check if course list has changed, if yes we deleted course and we shouldn't be here
    	  if (!courseMap.containsKey(course.getId())) {
    		  setResult(RESULT_OK, null);
    		  finish();
    	  } else {
    		  initFields();
    	  }
      }
      
	}
	
	// saves all notes by specified course
	public static void saveNotesData(String json_encoded) throws JSONException {
		JSONObject json = new JSONObject(json_encoded);
		JSONArray jarr = json.getJSONArray("notes");
		for (int i = 0; i < jarr.length(); i++) {
			JSONObject jNote = jarr.getJSONObject(i);
			((MyStorage)MyStorage.getAppContext()).saveNoteByJson(course, jNote);
		}
	}

	// saves all notes by specified course
	public static void saveEventsData(String json_encoded) throws JSONException {
		User user = ((MyStorage)MyStorage.getAppContext()).getUser();
		
		JSONObject json = new JSONObject(json_encoded);
		JSONArray jarr = json.getJSONArray("events");
		for (int i = 0; i < jarr.length(); i++) {
			JSONObject jEvent = jarr.getJSONObject(i);
			((MyStorage)MyStorage.getAppContext()).saveEventByJson(user, course, jEvent);
		}
	}

	public static void getNotesCallback(String notify) {
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				// static content will be called
				CreateNoteActivity.course = course;
				saveNotesData(json_encoded);
				
				Intent intent = new Intent(activity, NoteInterfaceActivity.class);
				intent.putExtra("courseId", courseId);
				activity.startActivity(intent);
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

    public static void getEventsCallback(String notify) {
    	Log.v("notify", notify);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				// static content will be called
				//CreateNoteActivity.course = course;
				Log.v("enc", json_encoded);
				EventInterfaceActivity.course = course;
				saveEventsData(json_encoded);
				
				Intent intent = new Intent(activity, EventInterfaceActivity.class);
				intent.putExtra("courseId", courseId);
				activity.startActivity(intent);
			} catch (JSONException e) {
				Log.v("exc", e.getMessage());
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
