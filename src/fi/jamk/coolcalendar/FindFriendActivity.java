package fi.jamk.coolcalendar;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.Note;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyFindFriendAdapter;
import fi.jamk.coolcalendar.util.MyStorage;

public class FindFriendActivity extends BaseActivity {

	private TextView tUserMsg;
	private EditText fName;
	private static ListView friendList;
	public static Button fSubmit;
	
	public static Context context;
	public static Activity activity;
	
	public static User user;
	private static ArrayList<String> userIds;
	
	// being set from fragment
	public static String courseId;
	
	//public static ArrayList<User> fUsers;
	//public static ArrayList<Course> fUserCourses;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_findfriend);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
		user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	fName = (EditText) findViewById(R.id.fName);
    	friendList = (ListView) findViewById(R.id.friendList);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    }
    
    private void initCallbacks() {

    	// ListView Item Click Listener
        friendList.setOnItemClickListener(new OnItemClickListener() {

              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) {
                
            	showDialog(userIds.get(position));
            	  
              }

         });

    	fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    private static void loadListView(ArrayList<User> users) {
    	ArrayList<String> userNames = new ArrayList<String>();
    	userIds = new ArrayList<String>();
    	for (User u : users) {
    		// we don't need ourselves
    		if (u.getId().equals(user.getId()))
    			continue;

    		userNames.add(u.getFullname());
    		userIds.add(u.getId());
    	}
    	
    	MyFindFriendAdapter adapter = new MyFindFriendAdapter(context, userNames, activity, userIds);
    	friendList.setAdapter(adapter);
    }
    
    private boolean validateFields() {
    	//getString(R.string.username_is_required)
    	
    	boolean valid = true;
    	
    	if (fName.getText().toString().length() == 0) {
    		fName.setError("Name is required.");
    		valid = false;
    	}
    	
    	return valid;
    }

    private void submit() {
    	if (validateFields()) {
	    	String retString = "";
	
	    	fSubmit.setEnabled(false);
	    	friendList.setEnabled(false);
	    	retString =
		    	DBService.getUsers(fName.getText().toString());
	
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	}
    }
    
    // prepares map for grabbing list of courses by user id
    private static void prepareCourseMap(ArrayList<Course> courses) {
    	Map<String, ArrayList<Course>> userCoursesMap = new HashMap<String, ArrayList<Course>>();
    	
    	for (Course c : courses) {
    		ArrayList<Course> userCourses = userCoursesMap.get(c.getUser().getId());
    		if (userCourses == null) {
    			userCourses = new ArrayList<Course>();
    		}
    		
    		userCourses.add(c);
    		userCoursesMap.put(c.getUser().getId(), userCourses);
    	}
    	
    	((MyStorage)MyStorage.getAppContext()).setUserCoursesMap(userCoursesMap);
    }

    public static void showDialog(String userId) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
        Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = FriendCourseFragment.newInstance(userId);
        newFragment.show(ft, "dialog");
    }

    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
		friendList.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);

			try {
				JSONObject json = new JSONObject(json_encoded);
				JSONArray jarr = json.getJSONArray("users");
				JSONArray jarrcr = json.getJSONArray("courses");

				ArrayList<User> users = ((MyStorage)MyStorage.getAppContext()).getUsersByJson(jarr);

		    	// convert array to map
		    	Map<String, User> usersMap = new HashMap<String, User>();
		    	for (User u : users) {
		    		usersMap.put(u.getId(), u);
		    	}
				ArrayList<Course> courses = ((MyStorage)MyStorage.getAppContext()).getCoursesByJson(jarrcr, usersMap);

				loadListView(users);
				prepareCourseMap(courses);
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
			//activity.setResult(RESULT_OK, null);
			//activity.finish();
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }
    
    public static void getCopyNotesCallback(String notify) {
		fSubmit.setEnabled(true);

		if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			// reset course variable
			Map<String, Map<String, Note> > notesMap = ((MyStorage)MyStorage.getAppContext()).getNotesMap();
			if (notesMap != null && notesMap.get(courseId) != null) {
				notesMap.put(courseId, null);
			}
			((MyStorage)MyStorage.getAppContext()).setNotesMap(notesMap);

			Toast.makeText(activity, "Notes copied.", Toast.LENGTH_LONG).show();

    	} else {
    		// something wrong
    		Toast.makeText(activity, notify, Toast.LENGTH_LONG).show();
    	}
    }
    
    public static void saveData(String json_encoded) throws JSONException {
	    JSONObject json = new JSONObject(json_encoded);
		JSONArray jcourses = null;
		
		if (json.has("courses") && !json.isNull("courses"))
			jcourses = json.getJSONArray("courses");

		if (jcourses != null)
			((MyStorage)MyStorage.getAppContext()).saveCoursesByJson(jcourses);
    }

    public static void getCopyCoursesCallback(String notify) {
		fSubmit.setEnabled(true);

    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
    		try {
	    		// save acquired data to storage
    			Log.v("Json encoded", json_encoded);
    			saveData(json_encoded);

	    		Toast.makeText(context, "Course copied.", Toast.LENGTH_LONG).show();	    		

	    		ArrayList<Course> cs = ((MyStorage)MyStorage.getAppContext()).getCourses();
	    		for (Course c : cs) {
	    			Log.v("c name", c.getName());
	    		}

	    		activity.setResult(RESULT_OK, null);
	    		activity.finish();
			} catch (JSONException e) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				Toast.makeText(context, sw.toString(), Toast.LENGTH_LONG).show();
				
			}
    	} else {
    		// something wrong
    		Toast.makeText(activity, notify, Toast.LENGTH_LONG).show();
    	}
    }

    

}
