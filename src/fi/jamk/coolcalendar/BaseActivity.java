package fi.jamk.coolcalendar;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

public class BaseActivity extends Activity {

	//private static Context context;
	private static Activity activity;
	protected int currentTheme;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //context = this;
        activity = this;
        
        int resId = loadTheme();
        setTheme(resId);
        currentTheme = resId;
    }
	
	private int loadTheme() {
		Context context = this.getApplicationContext();
		SharedPreferences sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);
		String theme = sharedPref.getString("theme", "Light");
		Log.v("theme", theme);
		
		int ret = android.R.style.Theme_Light;
		if (theme.equals("Black")) {
			ret = android.R.style.Theme_Black;
		}
		if (theme.equals("Holo")) {
			ret = android.R.style.Theme_Holo;
		}
		if (theme.equals("Holo_Light")) {
			ret = android.R.style.Theme_Holo_Light;
		}
		if (theme.equals("Translucent")) {
			ret = android.R.style.Theme_Translucent;
		}

		return ret;
	}
	
	public static void saveTheme(String theme) {
		//Context con = context.getApplicationContext();
		SharedPreferences sharedPref = activity.getSharedPreferences("shared", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("theme", theme);
		editor.commit();
	}

}
