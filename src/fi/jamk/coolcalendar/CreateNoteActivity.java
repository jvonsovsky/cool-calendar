package fi.jamk.coolcalendar;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.Note;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class CreateNoteActivity extends BaseActivity {

	private String courseId;
	public static Course course = null;
	private TextView tUserMsg;
	private EditText fNote;
	private DatePicker fDay;
	private CheckBox fSharable;
	public static Button fSubmit;
	
	// after first save, noteId is set to not null and we will use update method instead of create
	public static String noteId = null;

	public static Context context;
	public static Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_createnote);

		context = this;
		activity = this;
		noteId = null;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
    	Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			courseId = extras.getString("courseId");
			course = courseMap.get(courseId);
		}
		
		// this shouldn't happen
		if (course == null) {
			finish();
		}

    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	fNote = (EditText) findViewById(R.id.fNote);
    	fDay = (DatePicker) findViewById(R.id.fDay);
    	fSharable = (CheckBox) findViewById(R.id.fSharable);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    	
    	//tUserMsg.setText("Course " + course.getName());
    }
    
    private void initCallbacks() {

    	fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    private boolean validateFields() {
    	boolean valid = true;
    	
    	return valid;
    }

    private void submit() {
    	if (validateFields()) {
	    	String retString = "";
	
			// Why Calendar indexes months from zero?
	    	int month = fDay.getMonth() + 1;
	
	    	fSubmit.setEnabled(false);
	    	// new note
	    	if (noteId == null) {
	    		Log.v("info create", "creating");
				retString =
			    	DBService.createNote(course.getId(), fNote.getText().toString(), 
			    			fDay.getYear() + "-" + month + "-" + fDay.getDayOfMonth(), fSharable.isChecked() ? "on" : "off");
	
			// updating note
	    	} else {
	    		Log.v("info create", "updating");
	    		retString =
				    	DBService.updateNote(noteId, course.getId(), fNote.getText().toString(), 
				    			fDay.getYear() + "-" + month + "-" + fDay.getDayOfMonth(), 
				    			fSharable.isChecked() ? "on" : "off", "create");
	    	}
	
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	}
    }
    
    public static void saveNote(Note note) {
    	Map<String, Map<String, Note>> notes = ((MyStorage)MyStorage.getAppContext()).getNotesMap();
    	if (notes == null) {
    		notes = new HashMap<String, Map<String, Note>>();
    	}
    	
    	Map<String, Note> courseNotes = notes.get(course.getId());
    	if (courseNotes == null) {
    		courseNotes = new HashMap<String, Note>();
    	}
    	
    	// save as we are not leaving this activity yet
    	if (note.getId() != null) {
    		noteId = note.getId();
    	}
    	
    	courseNotes.put(note.getId(), note);
    	notes.put(course.getId(), courseNotes);
    	Log.v("I am in", "notes");
    	((MyStorage)MyStorage.getAppContext()).setNotesMap(notes);
    }
    
    public static void saveData(String json_encoded) throws JSONException {
	    JSONObject json = new JSONObject(json_encoded);
		JSONObject jnote = null;
		
		if (json.has("note") && !json.isNull("note"))
			jnote = json.getJSONObject("note");

		Log.v("savenote", json_encoded);
		try {
			if (jnote != null)
				((MyStorage)MyStorage.getAppContext()).saveNoteByJson(course, jnote);
		} catch (JSONException e) {
			Log.v("error", e.getMessage());
		}
    }

    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				saveData(json_encoded);
				fSubmit.setText("Update");
				Toast.makeText(context, "Note saved.", Toast.LENGTH_LONG).show();
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
