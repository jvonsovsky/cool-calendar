package fi.jamk.coolcalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.util.DBService;

public class EventFromServiceActivity extends BaseActivity {

	private TextView tUserMsg;
	private Button bSure;
	private static Button bDont;
	
	private String userId = null;
	private String fullname = null;
	private String courseId = null;
	private String eventId = null;
	private String description = null;
	private String deadline = null;
	private Long diff = 0L;
	
	private static Context context;
	private static Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eventservice);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	bSure = (Button) findViewById(R.id.bSure);
    	bDont = (Button) findViewById(R.id.bDont);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			userId = extras.getString("userId");
			fullname = extras.getString("fullname");
			courseId = extras.getString("courseId");
			eventId = extras.getString("eventId");
			description = extras.getString("description");
			deadline = extras.getString("deadline");
			diff = extras.getLong("diff");
		}
		
		//tUserMsg.setText("There are only " + diff + " Days left before your " + description);
		tUserMsg.setText(Html.fromHtml("<div align=\"center\">There are only<br /><h2>" + diff + " Days left</h2>before your<br />" + description + "</div>"));
		//Html.fromHtml("<h2>Title</h2><br><p>Description here</p>")
    }
    
    private void initCallbacks() {
    }
    
    public void onClickOkButton(View view) {
    	finish();
    }

    public void onClickCancelButton(View view) {
    	String retString = "";

    	bDont.setEnabled(false);
    	retString =
		    	DBService.updateServiceEvent(eventId, "off");

    	if (retString != "") {
    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
    	} else {
    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
    	}

    	updateEvent();
    }
    
    // update json in shared preferences
    private void updateEvent() {
		Context context = this.getApplicationContext();
		SharedPreferences sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);
		String eventsJson = sharedPref.getString("events", "{ users:  { } }");

		String jsonToSave = eventsJson;
		try {
			JSONObject json = new JSONObject(eventsJson);
			JSONObject jsonUsers = json.getJSONObject("users");
	        JSONObject jsonUserData = jsonUsers.getJSONObject(userId);
	        JSONObject jsonEvents = jsonUserData.getJSONObject("events");
			JSONArray jEvents = jsonEvents.getJSONArray(courseId);
			
			for (int i = 0; i < jEvents.length(); i++) {
				JSONObject jEvent = jEvents.getJSONObject(i);
				
				if (jEvent.getString("id").equals(eventId)) {
					jEvents.getJSONObject(i).put("countdown", false);
					
					break;
				}
			}
			
			jsonEvents.put(courseId, jEvents);
			jsonUserData.put("events", jsonEvents);
			jsonUsers.put(userId, jsonUserData);
			json.put("users", jsonUsers);
			
			jsonToSave = json.toString();
    	} catch (JSONException e) {
    		Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
    	}
		Log.v("save json as", jsonToSave);
		
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("events", jsonToSave);
		editor.commit();
    }

    public static void httpCallback(String notify) {
		bDont.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {
			activity.finish();
			Toast.makeText(context, "Event updated.", Toast.LENGTH_LONG).show();
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
