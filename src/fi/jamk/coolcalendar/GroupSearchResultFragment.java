package fi.jamk.coolcalendar;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import fi.jamk.coolcalendar.entity.Group;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.MyGroupSearchResultAdapter;
import fi.jamk.coolcalendar.util.MyStorage;

public class GroupSearchResultFragment extends DialogFragment {
    static String json_encoded;
    private List<Group> groups;
    private ArrayList<String> values;

	public static Activity activity;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    static GroupSearchResultFragment newInstance(String json_encoded) {
    	GroupSearchResultFragment f = new GroupSearchResultFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("json_encoded", json_encoded);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.json_encoded = getArguments().getString("json_encoded");

		activity = getActivity();

        //Toast.makeText(this, json_encoded, Toast.LENGTH_LONG).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        getDialog().setTitle("Search results");

    	View v = inflater.inflate(R.layout.fragment_groupsearchresult, container, false);
        TextView tv1 = (TextView)v.findViewById(R.id.textView1);
        ListView listView = (ListView)v.findViewById(R.id.listView);
        
        User user = ((MyStorage)MyStorage.getAppContext()).getUser();
        groups = new ArrayList<Group>();
        values = new ArrayList<String>();
        try {
    		Log.v("json_encoded", json_encoded);

    		JSONObject json = new JSONObject(json_encoded);
    		JSONArray jarr = json.getJSONArray("groups");
    		for (int i = 0; i < jarr.length(); i++) {
    			JSONObject jGroup = jarr.getJSONObject(i);
    			Log.v("group", jGroup.toString());
    			Group group = FindGroupActivity.saveGroupByJson(user, jGroup);

    			groups.add(group);
    	    	values.add(group.getName());
    		}    		
        } catch (JSONException e) {
        	Log.v("error", e.getMessage());
        	//getDialog().Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        
        Log.v("size", String.valueOf(groups.size()));
        MyGroupSearchResultAdapter adapter = new MyGroupSearchResultAdapter(activity, values, activity, groups);
        listView.setAdapter(adapter);

        return v;
    }


}