package fi.jamk.coolcalendar;

import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class CreateEventActivity extends BaseActivity {

	private String courseId;
	public static Course course = null;
	private TextView tUserMsg;
	private EditText fDesc;
	private DatePicker fDay;
	private CheckBox fCOn;
	public static Button fSubmit;
	
	public static Context context;
	public static Activity activity;
	
	public static User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_createevent);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
		user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
		Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			courseId = extras.getString("courseId");
			course = courseMap.get(courseId);
		}
		
		// this shouldn't happen
		if (course == null) {
			finish();
		}

    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	fDesc = (EditText) findViewById(R.id.fDesc);
    	fDay = (DatePicker) findViewById(R.id.fDay);
    	fCOn = (CheckBox) findViewById(R.id.fCOn);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    	
    	fCOn.setEnabled(true);
    	
    	//tUserMsg.setText("Course " + course.getName());
    }
    
    private void initCallbacks() {

    	fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    public static void saveData(String json_encoded) throws JSONException {
	    JSONObject json = new JSONObject(json_encoded);
		JSONObject jevent = null;
		
		if (json.has("event") && !json.isNull("event"))
			jevent = json.getJSONObject("event");

		Log.v("saveevent", json_encoded);
		try {
			if (jevent != null)
				((MyStorage)MyStorage.getAppContext()).saveEventByJson(user, course, jevent);
		} catch (JSONException e) {
			Log.v("error", e.getMessage());
		}
    }

    private boolean validateFields() {
    	boolean valid = true;
    	
    	if (fDesc.getText().toString().length() == 0) {
    		fDesc.setError("Description is required.");
    		valid = false;
    	}
    	
    	return valid;
    }
    
    private void submit() {
    	if (validateFields()) {
	    	String retString = "";
	
	    	fSubmit.setEnabled(false);
	    	retString =
		    	DBService.createEvent(user.getId(), course.getId(), fDesc.getText().toString(), 
		    			fDay.getYear() + "-" + fDay.getMonth() + "-" + fDay.getDayOfMonth(), 
		    			fCOn.isChecked() ? "on" : "off");
	
	
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	}
    }
    
    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				saveData(json_encoded);
				Toast.makeText(context, "Event saved.", Toast.LENGTH_LONG).show();
				activity.setResult(RESULT_OK, null);
				activity.finish();
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
