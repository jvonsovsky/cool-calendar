package fi.jamk.coolcalendar;

import java.util.Calendar;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.Note;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class DetailNoteActivity extends BaseActivity {

	private String courseId;
	public static Course course = null;
	private TextView tUserMsg;
	private EditText fNote;
	private DatePicker fDay;
	private CheckBox fSharable;
	public static Button fSubmit;
	
	// after first save, noteId is set to not null and we will use update method instead of create
	public static String noteId = null;
	private Note note;

	public static Context context;
	public static Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_createnote);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
    	Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			courseId = extras.getString("courseId");
			course = courseMap.get(courseId);
			noteId = extras.getString("noteId");
		}
		
		// this shouldn't happen
		if (course == null) {
			finish();
		}

    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	fNote = (EditText) findViewById(R.id.fNote);
    	fDay = (DatePicker) findViewById(R.id.fDay);
    	fSharable = (CheckBox) findViewById(R.id.fSharable);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    	
    	Map<String, Map<String, Note>> notes = ((MyStorage)MyStorage.getAppContext()).getNotesMap();
    	Map<String, Note> notesMap = notes.get(course.getId());
    	note = notesMap.get(noteId);
    	
    	fNote.setText(note.getText());
    	String timeZone = getString(R.string.time_zone);
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone( timeZone ));
    	cal.setTime(note.getDate());
    	fDay.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) - 1, cal.get(Calendar.DAY_OF_MONTH));
    	fSharable.setChecked( note.getSharable() );
    	fSubmit.setText("Update");
    }
    
    private void initCallbacks() {

    	fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    private void submit() {
    	String retString = "";

		// Why Calendar indexes months from zero?
    	int month = fDay.getMonth() + 1;

    	fSubmit.setEnabled(false);
    	retString =
		    	DBService.updateNote(noteId, course.getId(), fNote.getText().toString(), 
		    			fDay.getYear() + "-" + month + "-" + fDay.getDayOfMonth(), 
		    			fSharable.isChecked() ? "on" : "off", "detail");

    	if (retString != "") {
    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
    	} else {
    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
    	}
    }

    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				CreateNoteActivity.course = course;
				CreateNoteActivity.saveData(json_encoded);
				activity.setResult(RESULT_OK, null);
				fSubmit.setText("Update");
				Toast.makeText(context, "Note saved.", Toast.LENGTH_LONG).show();
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
