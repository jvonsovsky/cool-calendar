package fi.jamk.coolcalendar;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import fi.jamk.coolcalendar.entity.User;
import android.util.Base64;
import android.util.Log;

public class DBService {

	public static String urlPrefix = "http://shrouded-shelf-3037.herokuapp.com/";
	//public static String urlPrefix = "http://localhost:8080/";
	public static String httpResult;
	
	public static String callbackMethod = null;
	
	/*
	 * Returns Basic access authentication header
	 */
	public static String getAuthHeader() {
		User u = ((MyStorage) MyStorage.getAppContext()).getUser();
		
		return getAuthHeader(u.getUsername(), u.getPassword());
	}
	
	public static String getAuthHeader(String username, String password) {
		// String to be encoded with Base64
		String text = username + ":" + password;
		// Sending side
		byte[] data = null;
		try {
		    data = text.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
		    Log.v("Encoding error: ", e.getMessage());
		}

		String base64 = Base64.encodeToString(data, Base64.DEFAULT);
		String header = "Basic " + base64;
		header = header.trim();
		
		return header;
	}
	
	public static String createUser(String fullname, String username, String password, String email) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("fullname", fullname);
			jsonObj.put("username", username);
			jsonObj.put("password", password);
			jsonObj.put("email", email);
		} catch (JSONException e) {
			return e.getMessage();
		}

        //String retString = postData(urlPrefix + "update_user", nameValuePairs);
        new RequestTask().execute(urlPrefix + "create_user", jsonObj.toString(), null, "CreateUserActivity");
        //return retString;
        return "";
	}
	
	public static void logInUser(String username, String password) {
        new RequestTask().execute(urlPrefix + "log_in", null, getAuthHeader(username, password), "LogInUserActivity");
	}

	public static String createCourse(String userId, String name, String day, String start, String end) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("user", userId);
			jsonObj.put("name", name);
			jsonObj.put("day", day);
			jsonObj.put("start", start);
			jsonObj.put("end", end);
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "create_course", jsonObj.toString(), getAuthHeader(), "CreateCourseActivity");

        return "";
	}
	
	public static String updateCourse(String id, String userId, String name, String teacher, String day, String extraInfo, String start, String end) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		JSONObject inJsonObj = new JSONObject();
		try {
			jsonObj.put("id", id);
			
			inJsonObj.put("user", userId);
			inJsonObj.put("name", name);
			inJsonObj.put("teacher", teacher);
			inJsonObj.put("day", day);
			inJsonObj.put("extra_info", extraInfo);
			inJsonObj.put("start", start);
			inJsonObj.put("end", end);
			jsonObj.put("json", inJsonObj.toString());
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "update_course", jsonObj.toString(), getAuthHeader(), "DetailCourseActivity");

        return "";
	}

	public static String deleteCourse(String id) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		JSONObject inJsonObj = new JSONObject();
		try {
			jsonObj.put("id", id);
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "delete_course", jsonObj.toString(), getAuthHeader(), "DetailCourseActivity", "deletedCallback");

        return "";
	}
	
	public static String createNote(String courseId, String text, String day) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		String userId = ((MyStorage) MyStorage.getAppContext()).getUserId();
		try {
			jsonObj.put("author", userId);
			jsonObj.put("course", courseId);
			jsonObj.put("date", day);
			jsonObj.put("text", text);
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "create_note", jsonObj.toString(), getAuthHeader(), "CreateNoteActivity");

        return "";
	}
	
	public static String updateNote(String id, String courseId, String text, String day, String caller) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		JSONObject inJsonObj = new JSONObject();
		String userId = ((MyStorage) MyStorage.getAppContext()).getUserId();
		try {
			jsonObj.put("id", id);
			
			inJsonObj.put("_id", id);
			inJsonObj.put("author", userId);
			inJsonObj.put("course", courseId);
			inJsonObj.put("date", day);
			inJsonObj.put("text", text);
			jsonObj.put("json", inJsonObj.toString());
		} catch (JSONException e) {
			return e.getMessage();
		}

        if (caller.equals("detail")) {
        	new RequestTask().execute(urlPrefix + "update_note", jsonObj.toString(), getAuthHeader(), "DetailNoteActivity");
        } else new RequestTask().execute(urlPrefix + "update_note", jsonObj.toString(), getAuthHeader(), "CreateNoteActivity");

        return "";
	}

	public static String getNotes(String courseId) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("course", courseId);
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "find_notes/" + courseId, jsonObj.toString(), getAuthHeader(), "CourseInterfaceActivity", "getNotesCallback");

        return "";
	}
	
	public static String getEvents(String userId, String courseId) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("course", courseId);
		} catch (JSONException e) {
			return e.getMessage();
		}

        String url = urlPrefix + "find_events/" + userId + "/" + courseId;
        Log.v("url", url);
		new RequestTask().execute(url, jsonObj.toString(), getAuthHeader(), "CourseInterfaceActivity", "getEventsCallback");

        return "";
	}

	public static String deleteNote(String id) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		JSONObject inJsonObj = new JSONObject();
		try {
			jsonObj.put("id", id);
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "delete_note", jsonObj.toString(), getAuthHeader(), "DetailNoteActivity", "deletedCallback");

        return "";
	}
	
	public static String updateSettings(String userId, String cOn, String cTime, String color) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		JSONObject inJsonObj = new JSONObject();
		try {
			jsonObj.put("user", userId);
			
			inJsonObj.put("countdown_on", cOn);
			inJsonObj.put("countdown_timer", cTime);
			inJsonObj.put("color", color);
			jsonObj.put("json", inJsonObj.toString());
		} catch (JSONException e) {
			return e.getMessage();
		}

        Log.v("json string", jsonObj.toString());
		new RequestTask().execute(urlPrefix + "update_setting", jsonObj.toString(), getAuthHeader(), "SettingsInterfaceActivity");

        return "";
	}
	
	public static String createEvent(String userId, String courseId, String desc, String day, String cOn) {
        // Add your data
		JSONObject jsonObj = new JSONObject();

		try {
			jsonObj.put("user", userId);
			jsonObj.put("course", courseId);
			jsonObj.put("description", desc);
			jsonObj.put("deadline", day);
			jsonObj.put("countdown_on", cOn);
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "create_event", jsonObj.toString(), getAuthHeader(), "CreateEventActivity");

        return "";
	}

	public static String updateEvent(String id, String userId, String courseId, String desc, String day, String cOn) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		JSONObject inJsonObj = new JSONObject();

		try {
			jsonObj.put("id", id);
			
			inJsonObj.put("_id", id);
			inJsonObj.put("user", userId);
			inJsonObj.put("course", courseId);
			inJsonObj.put("description", desc);
			inJsonObj.put("deadline", day);
			inJsonObj.put("countdown_on", cOn);
			jsonObj.put("json", inJsonObj.toString());
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "update_event", jsonObj.toString(), getAuthHeader(), "DetailEventActivity");

        return "";
	}

	public static String createGroup(String userId, String name, String day, String start, String end) {
        // Add your data
		JSONObject jsonObj = new JSONObject();

		try {
			jsonObj.put("author", userId);
			jsonObj.put("name", name);
			jsonObj.put("day", day);
			jsonObj.put("start", start);
			jsonObj.put("end", end);
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "create_group", jsonObj.toString(), getAuthHeader(), "CreateGroupActivity");

        return "";
	}

	public static String updateGroup(String id, String userId, String name, String day, String start, String end) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		JSONObject inJsonObj = new JSONObject();

		try {
			jsonObj.put("id", id);
			
			inJsonObj.put("_id", id);
			inJsonObj.put("author", userId);
			inJsonObj.put("name", name);
			inJsonObj.put("day", day);
			inJsonObj.put("start", start);
			inJsonObj.put("end", end);
			jsonObj.put("json", inJsonObj.toString());
		} catch (JSONException e) {
			return e.getMessage();
		}

        new RequestTask().execute(urlPrefix + "update_group", jsonObj.toString(), getAuthHeader(), "DetailGroupActivity");

        return "";
	}

	public static String getGroups(String userId) {
        // Add your data
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("userid", userId);
			// service should look in binding table of many to many relation between user and group
			jsonObj.put("find_by_membership", "true");
		} catch (JSONException e) {
			return e.getMessage();
		}

        String url = urlPrefix + "find_groups";
        Log.v("url", url);
		new RequestTask().execute(url, jsonObj.toString(), getAuthHeader(), "UserInterfaceActivity", "getGroupsCallback");

        return "";
	}

	// calls callback method in predefined class
	public static void returnHttpData(String callbackClass) {
		String methodName = "httpCallback";
		Method m;

		if (callbackClass != null) {
			try {
				// construct and call proper method
				Class[] cArg = new Class[1];
				cArg[0] = String.class;
				Class c = Class.forName("fi.jamk.coolcalendar." + callbackClass);

				if (callbackMethod != null) {
					methodName = callbackMethod;
				}
				Log.v("Callback", "calling " + methodName + " in " + callbackClass);
				m = c.getMethod(methodName, cArg);
				m.invoke(null, httpResult);
			} catch (ClassNotFoundException e) {
				Log.v("Class exception: ", e.getMessage());
			} catch (NoSuchMethodException e) {
				Log.v("Method exception: ", e.getMessage());
			} catch (IllegalArgumentException e) {
				Log.v("Method exception: ", e.getMessage());
			} catch (IllegalAccessException e) {
				Log.v("Method exception: ", e.getMessage());
			} catch (InvocationTargetException e) {
				Log.v("Methods", callbackClass + ", " + methodName);
				//Log.v("Method exception: ", e.getMessage());
				
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				Log.v("err", sw.toString());
			}
			//CreateUserActivity.doNotification(httpResult);
		}
	}
	
	public static String postData(String url, List<NameValuePair> postData) {
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(url);
	    String responseBody;

	    try {
	        httppost.setEntity((HttpEntity) new UrlEncodedFormEntity(postData));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        HttpEntity entity = response.getEntity();
	        responseBody = EntityUtils.toString(entity);
	        
	    } catch (ClientProtocolException e) {
	    	responseBody = e.getMessage();
	    } catch (IOException e) {
	        responseBody = e.getMessage();
	    }
	    
	    return responseBody;
	} 

}
