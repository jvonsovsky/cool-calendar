package fi.jamk.coolcalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class CreateCourseActivity extends BaseActivity {

	private EditText fName;
	private Spinner fDay;
	private EditText fExtraInfo;
	private TimePicker tStart;
	private TimePicker tEnd;
	private CheckBox fCOn;
	public static Button fSubmit;
	
	public static Context context;
	public static Activity activity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_createcourse);

		context = this;
		activity = this;
		initFields();
		initCallbacks();
		
		fExtraInfo.setVisibility(View.GONE);
		fSubmit.setVisibility(View.VISIBLE);
	}

    private void initFields() {
    	fName = (EditText) findViewById(R.id.fName);
    	fDay = (Spinner) findViewById(R.id.fDay);
    	fExtraInfo = (EditText) findViewById(R.id.fExtraInfo);
    	tStart = (TimePicker) findViewById(R.id.tStart);
    	tEnd = (TimePicker) findViewById(R.id.tEnd);
    	fCOn = (CheckBox) findViewById(R.id.fCOn);
    	fSubmit = (Button) findViewById(R.id.fSubmit);

    	// create days spinner
    	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
    	        R.array.days_array, android.R.layout.simple_spinner_item);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	fDay.setAdapter(adapter);    
    	
    	fCOn.setEnabled(true);
    }
    
    private void initCallbacks() {
        fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }

    private boolean validateFields() {
    	//getString(R.string.username_is_required)
    	
    	boolean valid = true;
    	
    	if (fName.getText().toString().length() == 0) {
    		fName.setError("Name is required.");
    		valid = false;
    	}    	

    	return valid;
    }
    
    private void submit() {
    	if (validateFields()) {
    		String userId = ((MyStorage)MyStorage.getAppContext()).getUserId();
    		String retString =
		    	DBService.createCourse(userId, fName.getText().toString(), fDay.getSelectedItem().toString(),
		    			tStart.getCurrentHour() + ":" + tStart.getCurrentMinute(),
		    			tEnd.getCurrentHour() + ":" + tEnd.getCurrentMinute(), fCOn.isChecked() ? "on" : "off");
	    	fSubmit.setEnabled(false);
    		if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	}
    }
    
    public static void saveData(String json_encoded) throws JSONException {
	    JSONObject json = new JSONObject(json_encoded);
		JSONArray jcourses = null;
		
		if (json.has("courses") && !json.isNull("courses"))
			jcourses = json.getJSONArray("courses");

		if (jcourses != null)
			((MyStorage)MyStorage.getAppContext()).saveCoursesByJson(jcourses);
    }

    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				saveData(json_encoded);
				Toast.makeText(context, "Course created.", Toast.LENGTH_LONG).show();
				activity.setResult(RESULT_OK, null);
	    		activity.finish();
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
