package fi.jamk.coolcalendar;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyCourseAdapter;
import fi.jamk.coolcalendar.util.MyStorage;

public class UserInterfaceActivity extends BaseActivity {
	
	private TextView tUserMsg;
	private ListView listView;
	
	public static Activity activity;
	public static Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_userinterface);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	listView = (ListView) findViewById(R.id.courseList);
    	
    	User user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	tUserMsg.setText("Welcome " + user.getFullname());
    	
    	ArrayList<Course> courses = ((MyStorage)MyStorage.getAppContext()).getCourses();
    	ArrayList<String> values = new ArrayList<String>();
    	ArrayList<String> idValues = new ArrayList<String>();
    	for (Course course : courses) {
    		Log.v("g id", course.getId());
    		values.add(course.getName());
    		idValues.add(course.getId());
    	}
    	
    	Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();

	  	Iterator it = courseMap.entrySet().iterator();
		while (it.hasNext()) {
		    Map.Entry pairs = (Map.Entry)it.next();
		    Course c = (Course)pairs.getValue();
		    Log.v("course id", c.getId());
		    Log.v("course name", c.getName());
		    Log.v("key", (String)pairs.getKey());
		}

    	MyCourseAdapter adapter = new MyCourseAdapter(this, this, values, idValues, courseMap);
    	
    	listView.setAdapter(adapter);
    }
    
    private void initCallbacks() {

    	final ArrayList<Course> courses = ((MyStorage)MyStorage.getAppContext()).getCourses();

    	// ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {

              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) {
                
            	  Intent intent = new Intent(UserInterfaceActivity.this, CourseInterfaceActivity.class);
            	  intent.putExtra("courseId", courses.get(position).getId());
            	  startActivityForResult(intent, 1);
              }

         });
    }
    
    public void onClickLogoutButton(View view) {
    	((MyStorage)MyStorage.getAppContext()).setUserId("");
    	((MyStorage)MyStorage.getAppContext()).setUser(null);
    	((MyStorage)MyStorage.getAppContext()).setSettings(null);
    	((MyStorage)MyStorage.getAppContext()).setCourses(null);
    	((MyStorage)MyStorage.getAppContext()).setCourseMap(null);
    	((MyStorage)MyStorage.getAppContext()).setNotesMap(null);
    	((MyStorage)MyStorage.getAppContext()).setEventsMap(null);
    	((MyStorage)MyStorage.getAppContext()).setGroupsMap(null);
    	((MyStorage)MyStorage.getAppContext()).setMessagesMap(null);
    	
    	finish();
    }

    public void onClickSettingsButton(View view) {
		Intent intent = new Intent(UserInterfaceActivity.this, SettingsInterfaceActivity.class);
		startActivityForResult(intent, 1);
    }
    
    public void onClickCreateCourseButton(View view) {
		Intent intent = new Intent(UserInterfaceActivity.this, CreateCourseActivity.class);
		startActivityForResult(intent, 1);
    }
    
    public void onClickFindFriendsButton(View view) {
		Intent intent = new Intent(UserInterfaceActivity.this, FindFriendActivity.class);
		startActivityForResult(intent, 1);
    }

    public void onClickGroupsInterfaceButton(View view) {
    	// if data for this user weren't loaded yet
    	if (((MyStorage)MyStorage.getAppContext()).getGroupsMap() == null) {
	    	User user = ((MyStorage)MyStorage.getAppContext()).getUser();
	    	String retString = "";
	
			retString =	DBService.getGroups(user.getId());
	
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	} else {
    		// no need for grabbing data again
			Intent intent = new Intent(activity, GroupInterfaceActivity.class);
			activity.startActivity(intent);
    	}
    }

    public static void getGroupsCallback(String notify) {
    	Log.v("notify", notify);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				// static content will be called
				//CreateNoteActivity.course = course;
				Log.v("enc", json_encoded);
				saveGroupsData(json_encoded);
				
				Intent intent = new Intent(activity, GroupInterfaceActivity.class);
				activity.startActivity(intent);
			} catch (JSONException e) {
				Log.v("exc", e.getMessage());
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

	// saves all groups by specified course
	public static void saveGroupsData(String json_encoded) throws JSONException {
		User user = ((MyStorage)MyStorage.getAppContext()).getUser();
		
		JSONObject json = new JSONObject(json_encoded);
		JSONArray jarr = json.getJSONArray("groups");
		for (int i = 0; i < jarr.length(); i++) {
			JSONObject jGroup = jarr.getJSONObject(i);
			((MyStorage)MyStorage.getAppContext()).saveGroupByJson(user, jGroup);
		}
	}

    
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      if(resultCode == RESULT_OK){
    	  startActivity(getIntent());
    	  finish();
      }
	}

}
