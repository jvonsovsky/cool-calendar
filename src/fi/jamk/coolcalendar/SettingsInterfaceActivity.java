package fi.jamk.coolcalendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ParseException;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Settings;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class SettingsInterfaceActivity extends BaseActivity {

	private TextView textMsg;
	private CheckBox fCountdownOn;
	private EditText fCountdownTime;
	private Spinner fTheme;
	private Button fSubmit;
	
	public static Context context;
	public static Activity activity;
	
	public static User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settingsinterface);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}

    private void initFields() {
    	Settings settings = ((MyStorage)MyStorage.getAppContext()).getSettings();
    	
    	textMsg = (TextView) findViewById(R.id.textMsg);
    	fCountdownOn = (CheckBox) findViewById(R.id.fCountdownOn);
    	fCountdownTime = (EditText) findViewById(R.id.fCountdownTime);
    	fTheme = (Spinner) findViewById(R.id.fTheme);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    	
    	if (settings.getCountdown_on())
    		fCountdownOn.setChecked(true);
    	fCountdownTime.setText(String.valueOf(settings.getCountdown_timer()));
    	//fColor.setText(settings.getColor());
    	
    	user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
    	// create days spinner
    	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
    	        R.array.themes_array, android.R.layout.simple_spinner_item);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	fTheme.setAdapter(adapter);
    	
		SharedPreferences sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);
		String theme = sharedPref.getString("theme", "Light");
    	
    	int position = getDaySelection(theme);
    	fTheme.setSelection(position);
    	
    	textMsg.setText("Settings for " + user.getFullname());
    }
    
    private int getDaySelection(String themeStr) {
    	Resources res = getResources();
    	String[] themes = res.getStringArray(R.array.themes_array);
    	
    	int i = 0;
    	for (String theme : themes) {
    		if (theme.equals(themeStr)) {
    			return i;
    		}
    		i++;
    	}
    	
    	return 0;
    }

    private void initCallbacks() {

    	fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    private boolean validateFields() {
    	boolean valid = true;
    	
    	if (fCountdownTime.getText().toString().length() == 0) {
    		fCountdownTime.setError("Name is required.");
    		valid = false;
    	}
    	
    	if (valid) {
	    	try {
	    		Integer.parseInt(fCountdownTime.getText().toString());
	    	} catch (ParseException e) {
	    		fCountdownTime.setError("Value is not valid number.");
	    		valid = false;
	    	}
    	}

    	return valid;
    }
    
    private void submit() {
    	if (validateFields()) {
    		String retString = "";

    		retString =
    				DBService.updateSettings(user.getId(), fCountdownOn.isChecked() ? "on" : "off", 
    						fCountdownTime.getText().toString(), fTheme.getSelectedItem().toString());

    		if (retString != "") {
    			Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
    		} else {
    			Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
    		}
    	}
    }
    
    public static void saveData(String json_encoded) throws JSONException {
    	JSONObject json = new JSONObject(json_encoded);
    	JSONObject jsettings = json.getJSONObject("settings");
    	((MyStorage)MyStorage.getAppContext()).saveSettingsByJson(jsettings);
    }

    public static void httpCallback(String notify) {
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				saveData(json_encoded);
				Toast.makeText(context, "Settings saved.", Toast.LENGTH_LONG).show();
				activity.setResult(RESULT_OK, null);
				activity.finish();
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
