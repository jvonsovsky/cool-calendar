package fi.jamk.coolcalendar;

import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class CreateGroupActivity extends BaseActivity {

	private TextView tUserMsg;
	private EditText fName;
	private EditText fTeacher;
	private Spinner fDay;
	private TimePicker fStart;
	private TimePicker fEnd;
	public static Button fSubmit;
	
	public static Context context;
	public static Activity activity;
	
	public static User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_creategroup);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
		user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	fName = (EditText) findViewById(R.id.fName);
    	fTeacher = (EditText) findViewById(R.id.fTeacher);
    	fDay = (Spinner) findViewById(R.id.fDay);
    	fStart = (TimePicker) findViewById(R.id.fStart);
    	fEnd = (TimePicker) findViewById(R.id.fEnd);
    	fSubmit = (Button) findViewById(R.id.fSubmit);

    	// create days spinner
    	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
    	        R.array.days_array, android.R.layout.simple_spinner_item);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	fDay.setAdapter(adapter);
    	
    	String timeZone = getString(R.string.time_zone);
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone( timeZone ));
    	fStart.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
    	fEnd.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
    }
    
    private void initCallbacks() {

    	fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    public static void saveData(String json_encoded) throws JSONException {
	    JSONObject json = new JSONObject(json_encoded);
		JSONObject jgroup = null;
		
		if (json.has("group") && !json.isNull("group"))
			jgroup = json.getJSONObject("group");

		Log.v("savegroup", json_encoded);
		try {
			if (jgroup != null)
				((MyStorage)MyStorage.getAppContext()).saveGroupByJson(user, jgroup);
		} catch (JSONException e) {
			Log.v("error", e.getMessage());
		}
    }

    private boolean validateFields() {
    	boolean valid = true;
    	
    	if (fName.getText().toString().length() == 0) {
    		fName.setError("Name is required.");
    		valid = false;
    	}
    	
    	return valid;
    }
    
    private void submit() {
    	if (validateFields()) {
	    	String retString = "";
	
	    	fSubmit.setEnabled(false);
	    	retString =
		    	DBService.createGroup(user.getId(), fName.getText().toString(), fTeacher.getText().toString(),
		    			fDay.getSelectedItem().toString(), fStart.getCurrentHour() + ":" + fStart.getCurrentMinute(),
		    			fEnd.getCurrentHour() + ":" + fEnd.getCurrentMinute());
	
	
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	}
    }
    
    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				saveData(json_encoded);
				Toast.makeText(context, "Group saved.", Toast.LENGTH_LONG).show();
				activity.setResult(RESULT_OK, null);
				activity.finish();
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
