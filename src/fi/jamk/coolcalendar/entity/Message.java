package fi.jamk.coolcalendar.entity;

public class Message {
	private String id;
	private Group group;
	private User user;
	private String timestamp;
	private String text;

	public Message() {}
	
	public Message(String id, Group group, User user, String timestamp, String text) {
		this.id = id;
		this.group = group;
		this.user = user;
		this.timestamp = timestamp;
		this.text = text;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
