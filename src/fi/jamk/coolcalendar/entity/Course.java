package fi.jamk.coolcalendar.entity;

public class Course {
	private String id;
	private User user;
	private String name;
	private String teacher;
	private String day;
	private String start; // SQL time
	private String end; // SQL time
	private String extra_info;
	private Boolean countdown_on;

	public Course() {}
	
	public Course(String id, User user, String name, String teacher, String day, String start, String end, String extra_info, Boolean countdown_on) {
		this.id = id;
		this.user = user;
		this.name = name;
		this.teacher = teacher;
		this.day = day;
		this.start = start;
		this.end = end;
		this.extra_info = extra_info;
		this.countdown_on = countdown_on;
	}
	
	public User getUser() {
		return user;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getExtra_info() {
		return extra_info;
	}

	public void setExtra_info(String extra_info) {
		this.extra_info = extra_info;
	}
	
	public Boolean getCountdown_on() {
		return countdown_on;
	}

	public void setCountdown_on(Boolean countdown_on) {
		this.countdown_on = countdown_on;
	}

}
