package fi.jamk.coolcalendar.entity;

import java.util.Date;

public class Event {
	private String id;
	private User user;
	private Course course;
	private String description;
	private Date deadline;
	private Boolean countdown_on;

	public Event() {}
	
	public Event(String id, User user, Course course, String description, Date deadline, Boolean countdown_on) {
		this.id = id;
		this.user = user;
		this.course = course;
		this.description = description;
		this.deadline = deadline;
		this.countdown_on = countdown_on;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public Boolean getCountdown_on() {
		return countdown_on;
	}

	public void setCountdown_on(Boolean countdown_on) {
		this.countdown_on = countdown_on;
	}
}
