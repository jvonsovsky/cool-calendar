package fi.jamk.coolcalendar.entity;

import java.util.Date;

public class Note {
	private String id;
	private String user;  
	private String author;
	private String authorname;
	private Course course;
	private Date date;
	private String text;
	private Boolean sharable;

	public Note() {}
	
	public Note(String id, String user, String author, String authorname, Course course, Date date, String text, Boolean sharable) {
		this.id = id;
		this.user = user;
		this.author = author;
		this.authorname = authorname;
		this.course = course;
		this.date = date;
		this.text = text;
		this.sharable = sharable;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean getSharable() {
		return sharable;
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getAuthorname() {
		return authorname;
	}

	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}

	public void setSharable(Boolean sharable) {
		this.sharable = sharable;
	}
	
}
