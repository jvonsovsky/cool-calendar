package fi.jamk.coolcalendar.entity;

public class Settings {
	private User user;
	private Boolean countdown_on;
	private int countdown_timer;
	private String theme;

	public Settings() {}
	
	public Settings(User user, Boolean c_on, int c_tim, String theme) {
		this.user = user;
		this.countdown_on = c_on;
		this.countdown_timer = c_tim;
		this.theme = theme;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getCountdown_on() {
		return countdown_on;
	}

	public void setCountdown_on(Boolean countdown_on) {
		this.countdown_on = countdown_on;
	}

	public int getCountdown_timer() {
		return countdown_timer;
	}

	public void setCountdown_timer(int countdown_timer) {
		this.countdown_timer = countdown_timer;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

}
