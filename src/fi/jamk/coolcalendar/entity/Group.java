package fi.jamk.coolcalendar.entity;

public class Group {
	private String id;
	private User author;
	private String name;
	private String teacher;
	private String day;
	private String start;
	private String end;

	public Group() {}
	
	public Group(String id, User author, String name, String teacher, String day, String start, String end) {
		this.id = id;
		this.author = author;
		this.name = name;
		this.teacher = teacher;
		this.day = day;
		this.start = start;
		this.end = end;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}
	
	
}
