package fi.jamk.coolcalendar;

import java.util.Map;

import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Group;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class DetailGroupActivity extends BaseActivity {

	private TextView tUserMsg;
	private EditText fName;
	private EditText fTeacher;
	private Spinner fDay;
	private TimePicker fStart;
	private TimePicker fEnd;
	public static Button fSubmit;
	
	// after first save, noteId is set to not null and we will use update method instead of create
	public static String groupId = null;
	private Group group;

	public static Context context;
	public static Activity activity;
	
	private static User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_creategroup);

		context = this;
		activity = this;
		
		initFields();
		initValues();
		initCallbacks();
	}


    private void initFields() {
    	user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			groupId = extras.getString("groupId");
		}
		
    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	fName = (EditText) findViewById(R.id.fName);
    	fTeacher = (EditText) findViewById(R.id.fTeacher);
    	fDay = (Spinner) findViewById(R.id.fDay);
    	fStart = (TimePicker) findViewById(R.id.fStart);
    	fEnd = (TimePicker) findViewById(R.id.fEnd);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    	
    	// create days spinner
    	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
    	        R.array.days_array, android.R.layout.simple_spinner_item);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	fDay.setAdapter(adapter);
    	
    	Map<String, Group> courseMap = ((MyStorage)MyStorage.getAppContext()).getGroupsMap();
    	group = courseMap.get(groupId);
    }
    
    private int getDaySelection(String dayStr) {
    	Resources res = getResources();
    	String[] days = res.getStringArray(R.array.days_array);
    	
    	int i = 0;
    	for (String day : days) {
    		if (day.equals(dayStr)) {
    			return i;
    		}
    		i++;
    	}
    	
    	return 0;
    }
    
    private void initValues() {
    	String strStart = group.getStart();
    	String[] parts = strStart.split(":");
    	if (parts.length >= 2) {
    		fStart.setCurrentHour(Integer.valueOf(parts[0]));
    		fStart.setCurrentMinute(Integer.valueOf(parts[1]));
    	}
    	
    	String strEnd = group.getEnd();
    	parts = strEnd.split(":");
    	if (parts.length >= 2) {
    		fEnd.setCurrentHour(Integer.valueOf(parts[0]));
    		fEnd.setCurrentMinute(Integer.valueOf(parts[1]));
    	}

    	int position = getDaySelection(group.getDay());
    	
    	fName.setText(group.getName());
    	fTeacher.setText(group.getTeacher());
    	fDay.setSelection(position);
    	fSubmit.setText("Update");
    }
    
    private void initCallbacks() {

    	fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    private void submit() {
    	String retString = "";

    	fSubmit.setEnabled(false);
    	retString =
		    	DBService.updateGroup(groupId, user.getId(), fName.getText().toString(), fTeacher.getText().toString(),
		    			fDay.getSelectedItem().toString(), fStart.getCurrentHour() + ":" + fStart.getCurrentMinute(),
		    			fEnd.getCurrentHour() + ":" + fEnd.getCurrentMinute());

    	if (retString != "") {
    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
    	} else {
    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
    	}
    }

    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				CreateGroupActivity.user = user;
				CreateGroupActivity.saveData(json_encoded);
				activity.setResult(RESULT_OK, null);
				activity.finish();
				Toast.makeText(context, "Group saved.", Toast.LENGTH_LONG).show();
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
