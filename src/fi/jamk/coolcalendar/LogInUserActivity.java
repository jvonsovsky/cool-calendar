package fi.jamk.coolcalendar;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.FormUtils;
import fi.jamk.coolcalendar.util.MyStorage;

public class LogInUserActivity extends BaseActivity {

	private EditText fUsername;
	private EditText fPassword;
	public static Button fSubmit;
	
	public static Context context;
	public static Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loginuser);

		context = this;
		activity = this;
		initFields();
		initCallbacks();
	}


    private void initFields() {
    	fUsername = (EditText) findViewById(R.id.fUsername);
    	fPassword = (EditText) findViewById(R.id.fPassword);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    }
    
    private void initCallbacks() {
        // set on last edit text
    	fPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submit();
                    return true;
                }
                return false;
            }

        });

        fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }

    private boolean validateFields() {
    	//getString(R.string.username_is_required)
    	
    	boolean valid = true;
    	
    	if (fUsername.getText().toString().length() == 0) {
    		fUsername.setError("Name is not filled.");
    		valid = false;
    	}
    	
    	if (fPassword.getText().toString().length() == 0) {
    		fPassword.setError("Password is not filled.");
    		valid = false;
    	}

    	return valid;
    }

    private void submit() {
    	if (validateFields()) {
	    	FormUtils.hideKeyboard(context, fPassword);
	    	DBService.logInUser(fUsername.getText().toString(),	fPassword.getText().toString());
	    	fSubmit.setEnabled(false);
	
			Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
    	}
    }
    
    public static void saveData(String json_encoded) throws JSONException {
	    JSONObject json = new JSONObject(json_encoded);
		JSONObject jsettings = null;
		JSONObject juser = null;
		JSONArray jcourses = null;
		
		if (json.has("user") && !json.isNull("user"))
			juser = json.getJSONObject("user");
		if (json.has("settings") && !json.isNull("settings"))
			jsettings = json.getJSONObject("settings");
		if (json.has("courses") && !json.isNull("courses"))
			jcourses = json.getJSONArray("courses");

		((MyStorage)MyStorage.getAppContext()).saveUserByJson(juser);
		if (jsettings != null)
			((MyStorage)MyStorage.getAppContext()).saveSettingsByJson(jsettings);
		if (jcourses != null)
			((MyStorage)MyStorage.getAppContext()).saveCoursesByJson(jcourses);
    }
    
    public static void httpCallback(String notify) {
    	fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {
    		// if logging in was successful let's store user's id
    		String json_encoded = notify.substring(8);
    		try {
	    		// save acquired data to storage
    			Log.v("Json encoded", json_encoded);
    			saveData(json_encoded);

	    		Toast.makeText(context, "Logged In.", Toast.LENGTH_LONG).show();
	    		activity.setResult(RESULT_OK, null);
	    		activity.finish();
			} catch (JSONException e) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				Toast.makeText(context, sw.toString(), Toast.LENGTH_LONG).show();
				
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
