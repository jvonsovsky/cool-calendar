package fi.jamk.coolcalendar;

import java.util.Calendar;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.Event;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class DetailEventActivity extends BaseActivity {

	private String courseId;
	public static Course course = null;
	private TextView tUserMsg;
	private EditText fDesc;
	private DatePicker fDay;
	private CheckBox fCOn;
	public static Button fSubmit;
	
	// after first save, noteId is set to not null and we will use update method instead of create
	public static String eventId = null;
	private Event event;

	public static Context context;
	public static Activity activity;
	
	private static User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_createevent);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
    	user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
    	Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			courseId = extras.getString("courseId");
			course = courseMap.get(courseId);
			eventId = extras.getString("eventId");
		}
		
		// this shouldn't happen
		if (course == null) {
			finish();
		}

    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	fDesc = (EditText) findViewById(R.id.fDesc);
    	fDay = (DatePicker) findViewById(R.id.fDay);
    	fCOn = (CheckBox) findViewById(R.id.fCOn);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    	
    	Map<String, Map<String, Event>> events = ((MyStorage)MyStorage.getAppContext()).getEventsMap();
    	Map<String, Event> cMap = events.get(course.getId());
    	event = cMap.get(eventId);
    	
    	fDesc.setText(event.getDescription());
    	String timeZone = getString(R.string.time_zone);
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone( timeZone ));
    	cal.setTime(event.getDeadline());
    	fDay.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
    	fCOn.setChecked( event.getCountdown_on() );
    	fSubmit.setText("Update");
    }
    
    private void initCallbacks() {

    	fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    private void submit() {
    	String retString = "";

    	fSubmit.setEnabled(false);
    	retString =
		    	DBService.updateEvent(eventId, user.getId(), course.getId(), fDesc.getText().toString(), 
		    			fDay.getYear() + "-" + fDay.getMonth() + "-" + fDay.getDayOfMonth(), 
		    			fCOn.isChecked() ? "on" : "off");

    	if (retString != "") {
    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
    	} else {
    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
    	}
    }

    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				CreateEventActivity.user = user;
				CreateEventActivity.course = course;
				CreateEventActivity.saveData(json_encoded);
				activity.setResult(RESULT_OK, null);
				activity.finish();
				Toast.makeText(context, "Event saved.", Toast.LENGTH_LONG).show();
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
