package fi.jamk.coolcalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Group;
import fi.jamk.coolcalendar.entity.Message;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.FormUtils;
import fi.jamk.coolcalendar.util.MyStorage;

public class ChatGroupActivity extends BaseActivity {

	private TextView tUserMsg;
	private static Button fLeave;
	private static Button fDelete;
	private static EditText eMessage;
	private static Button fSubmit;
	
	public static Activity activity;
	public static Context context;
	
	private static Group group = null;
	private static User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_chatgroup);

		activity = this;
		context = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
    	user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
		// get group
    	Map<String, Group> groups = ((MyStorage)MyStorage.getAppContext()).getGroupsMap();
    	Bundle extras = getIntent().getExtras();
		if (extras != null) {
			String groupId = extras.getString("groupId");
			Log.v("groupid", groupId);
			group = groups.get(groupId);
		}
		Log.v("is null", String.valueOf(group == null));
    	
    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	fLeave = (Button) findViewById(R.id.fLeave);
    	fDelete = (Button) findViewById(R.id.fDelete);
    	eMessage = (EditText) findViewById(R.id.eMessage);
    	fSubmit = (Button) findViewById(R.id.fSubmit);
    	
    	// user cannot delete group if he didn't create it
    	if (!user.getId().equals( group.getAuthor().getId() )) {
    		fDelete.setVisibility(View.GONE);
    	}
    	
    	tUserMsg.setText("Group chat of " + group.getName());
    	
    	loadMessages();
    }
    
    private void initCallbacks() {
        // set on last edit text
    	eMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    submit();
                    return true;
                }
                return false;
            }

        });

        fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });

    }
    
    private void loadMessages() {
    	Map<String, Map<String, Message>> messages = ((MyStorage)MyStorage.getAppContext()).getMessagesMap();
    	Map<String, ArrayList<String>> messageIds = ((MyStorage)MyStorage.getAppContext()).getMessagesOrderedList();
    	
    	if (messages != null && messageIds != null) {
    		Map<String, Message> groupMessages = messages.get(group.getId());
    		ArrayList<String> groupMessageIds = messageIds.get(group.getId());
    		
    		if (groupMessageIds != null) {
	    		for (String messageId : groupMessageIds) {
	    			Message message = groupMessages.get(messageId);
	    			addMessageToUI(message);
	    		}
    		}
    	}
    }
    
    private static void addMessageToUI(Message message) {
        Drawable myBox = activity.getResources().getDrawable(R.drawable.my_box);
        LinearLayout ll = (LinearLayout) activity.findViewById(R.id.fMessages);

        // Add date
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat toFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        String timeMsg = "";
        try {
			Date d1 = format.parse( message.getTimestamp() );
			timeMsg = toFormat.format(d1);
		} catch (ParseException e) {
			timeMsg = "Unknown time";
		}
        
        TextView tv1 = new TextView(activity);
        tv1.setText(timeMsg);
        tv1.setPadding(10, 0, 10, 0);

        // Add name
        TextView tv2 = new TextView(activity);
        tv2.setText(message.getUser().getFullname());
        tv2.setTextColor(Color.rgb(112, 146, 190));
        tv2.setPadding(10, 0, 10, 0);

        // Add text
        TextView tv3 = new TextView(activity);
        tv3.setText(message.getText());
        tv3.setBackground(myBox);
        tv3.setPadding(10, 10, 10, 10);
        tv3.setMinimumWidth(250);

        // set margin
        LinearLayout.LayoutParams lParams1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lParams1.setMargins(0, 0, 0, 0);
        tv1.setLayoutParams(lParams1);
        ll.addView(tv1);

        LinearLayout.LayoutParams lParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lParams2.setMargins(0, 0, 0, 0);
        tv2.setLayoutParams(lParams2);
        ll.addView(tv2);

        LinearLayout.LayoutParams lParams3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lParams3.setMargins(0, 0, 0, 20);
        tv3.setLayoutParams(lParams3);
        ll.addView(tv3);
    }
    
    private boolean validateFields() {
    	boolean valid = true;
    	
    	if (eMessage.getText().toString().length() == 0) {
    		eMessage.setError("You didn't write anything.");
    		valid = false;
    	}
    	
    	return valid;
    }
    
    private void submit() {
    	if (validateFields()) {
    		String timeZone = getString(R.string.time_zone);
	    	Calendar c = Calendar.getInstance( TimeZone.getTimeZone( timeZone ) );
	    	int month = c.get(Calendar.MONTH) + 1;
	
	    	FormUtils.hideKeyboard(context, eMessage);
	    	fSubmit.setEnabled(false);
	    	String retString =
		    	DBService.createMessage(user.getId(), group.getId(), 
		    			c.get(Calendar.YEAR) + "-" + month + "-" + c.get(Calendar.DAY_OF_MONTH), 
		    			c.get(Calendar.HOUR) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND), eMessage.getText().toString());
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	}
    }
    
    public static void saveMessage(Message message) {
    	Map<String, Map<String, Message>> messages = ((MyStorage)MyStorage.getAppContext()).getMessagesMap();
    	if (messages == null) {
    		messages = new HashMap<String, Map<String, Message>>();
    	}
    	
    	Map<String, Message> courseMessages = messages.get(group.getId());
    	if (courseMessages == null) {
    		courseMessages = new HashMap<String, Message>();
    	}
    	
    	courseMessages.put(message.getId(), message);
    	messages.put(group.getId(), courseMessages);
    	Log.v("I am in", "messages");
    	((MyStorage)MyStorage.getAppContext()).setMessagesMap(messages);
    	
    	addMessageToUI(message);
    }

    public static void saveData(String json_encoded) throws JSONException {
	    JSONObject json = new JSONObject(json_encoded);
		JSONObject jmessage = null;
		
		if (json.has("message") && !json.isNull("message"))
			jmessage = json.getJSONObject("message");

		Log.v("savemessage", json_encoded);
		try {
			if (jmessage != null)
				((MyStorage)MyStorage.getAppContext()).saveMessageByJson(user, group, jmessage);
		} catch (JSONException e) {
			Log.v("error", e.getMessage());
		}
    }
    
    // callback for sending a message
    public static void httpCallback(String notify) {
    	fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				saveData(json_encoded);
			} catch (JSONException e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }
    
    public void onClickLeaveGroupButton(View view) {
	    final String userId = ((MyStorage)MyStorage.getAppContext()).getUserId();
    	
    	new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Leave group")
        .setMessage("Are you sure you want to leave this group?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	    		String retString =
	    		    	DBService.leaveGroup(userId, group.getId());
    	    	if (retString != "") {
    	    		Toast.makeText(context, retString, Toast.LENGTH_LONG).show();
    	    	} else {
    	    		Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
    	    	}

	        }
	
	    })
	    .setNegativeButton("No", null)
	    .show();
    }

    public void onClickDeleteGroupButton(View view) {
	    final String userId = ((MyStorage)MyStorage.getAppContext()).getUserId();
    	
    	new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Delete group")
        .setMessage("Are you sure you want to delete this group? All messages will be lost.")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
	    {
	        @Override
	        public void onClick(DialogInterface dialog, int which) {
	    		String retString =
	    		    	DBService.deleteGroup(group.getId());
    	    	if (retString != "") {
    	    		Toast.makeText(context, retString, Toast.LENGTH_LONG).show();
    	    	} else {
    	    		Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
    	    	}

	        }
	
	    })
	    .setNegativeButton("No", null)
	    .show();
    }
    
    public static void deleteFromGroup(String groupId) {
    	Map<String, Group> groups = ((MyStorage)MyStorage.getAppContext()).getGroupsMap();
    	
    	if (groups != null) {
    		groups.remove(groupId);
    	}
    	
    	((MyStorage)MyStorage.getAppContext()).setGroupsMap(groups);
    }
    
    public static void deletedCallback(String notify) {
		fDelete.setEnabled(true);
		fLeave.setEnabled(true);

		if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			activity.setResult(RESULT_OK, null);
			activity.finish();
			deleteFromGroup(group.getId());
			Toast.makeText(context, "Group deleted.", Toast.LENGTH_LONG).show();
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }
    
    public static void leaveGroupCallback(String notify) {
		fDelete.setEnabled(true);
		fLeave.setEnabled(true);

		if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			deleteFromGroup(group.getId());
			activity.setResult(RESULT_OK, null);
			Toast.makeText(context, "Group deleted.", Toast.LENGTH_LONG).show();
			activity.finish();
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      if(resultCode == RESULT_OK) {
    	  initFields();
    	  initCallbacks();
      }
	}

}
