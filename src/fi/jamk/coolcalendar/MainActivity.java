package fi.jamk.coolcalendar;

import org.json.JSONException;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import fi.jamk.coolcalendar.service.EventCourseService;
import fi.jamk.coolcalendar.util.MyStorage;

public class MainActivity extends BaseActivity {

    private Intent intentServiceEvent;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intentServiceEvent = new Intent(this, EventCourseService.class);
        startService(intentServiceEvent);
    }

    @Override
    protected void onDestroy() {
    	super.onDestroy();
    	//stopService(intentServiceEvent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
	public void onClickSignUpButton(View view) {
		Intent intent = new Intent(MainActivity.this, CreateUserActivity.class);
		startActivity(intent);
	}

	public void onClickSignInButton(View view) {
		Intent intent = new Intent(MainActivity.this, LogInUserActivity.class);
		startActivityForResult(intent, 1);
	}
	
	// testing purposes
	public void onClickLoggedInButton(View view) {
		//((MyStorage)MyStorage.getAppContext()).setUserId("546dea8cfb612d0002374d39");
		String userId = ((MyStorage) getApplicationContext()).getUserId();
		String json_encoded = "{\"settings\": null, \"courses\": " + 
					"[{\"extra_info\": \"No extra info\", \"end\": \"10:40\", \"name\": \"Android Rap\", \"teacher\": \"Pasi\", \"day\": \"Monday\", \"_id\": \"546f8bb83f946919501437cf\", \"start\": \"10:30\", \"user\": \"546dea8cfb612d0002374d39\"}, " + 
					"{\"day\": \"Tuesday\", \"end\": \"21:6\", \"name\": \"N Building\", \"teacher\": \"\", \"extra_info\": \"\", \"_id\": \"546fa94b3da9c30002fb1fd8\", \"user\": \"546dea8cfb612d0002374d39\", \"start\": \"21:6\"}], " + 
				"\"user\": {\"_id\": \"546dea8cfb612d0002374d39\", \"email\": \"\", \"fullname\": \"cc\", \"username\": \"cc\", \"password\": \"cc\"}}";
		try {
			if (userId == null) {
				LogInUserActivity.saveData(json_encoded);
				Log.v("Info", "Using test data");
			} else {
				Log.v("Info", "Using logged in data");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Intent intent = new Intent(MainActivity.this, UserInterfaceActivity.class);
		startActivity(intent);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      if(resultCode == RESULT_OK) {
    	  String userId = ((MyStorage) getApplicationContext()).getUserId();
    	  if (userId != null) {
			Intent intent = new Intent(MainActivity.this, UserInterfaceActivity.class);
			startActivity(intent);
    	  }
      }
      
	}


}
