package fi.jamk.coolcalendar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Group;
import fi.jamk.coolcalendar.entity.Message;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyGroupInterfaceAdapter;
import fi.jamk.coolcalendar.util.MyStorage;

public class GroupInterfaceActivity extends BaseActivity {

	private TextView tUserMsg;
	private static ListView listView;
	
	public static Context context;
	public static Activity activity;
	
	public static String groupId = null;

	User user;

	// ids from groups map will be stored here so we can get id by listview position
	private List<String> groupIds;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_groupinterface);

		context = this;
		activity = this;

		initFields();
		initCallbacks();
	}


    private void initFields() {
    	user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	listView = (ListView) findViewById(R.id.groupList);
    	
    	tUserMsg.setText("Groups review");
    	
    	// get groups
    	groupIds = new ArrayList<String>();
    	ArrayList<String> values = new ArrayList<String>();
    	Map<String, Group> groups = ((MyStorage)MyStorage.getAppContext()).getGroupsMap();

    	if (groups != null) {
	    	Iterator it = groups.entrySet().iterator();
	        while (it.hasNext()) {
	            Map.Entry pairs = (Map.Entry)it.next();
	            groupIds.add((String)pairs.getKey());
	            Group group = (Group)pairs.getValue();
	            values.add(group.getName());
	        }
    	}

    	MyGroupInterfaceAdapter adapter = new MyGroupInterfaceAdapter(context, values, activity, groupIds);
    	
    	listView.setAdapter(adapter);
    }
    
    private void initCallbacks() {

    	
    	// ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {

              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) {
                
              	// if data for this course weren't loaded yet
            	String gId = groupIds.get(position);

      	    	String retString = "";
      	
      			listView.setEnabled(false);
      			groupId = gId;
      	    	retString =	DBService.getMessages(gId);
      	
      	    	if (retString != "") {
      	    		Toast.makeText(context, retString, Toast.LENGTH_LONG).show();
      	    	} else {
      	    		Toast.makeText(context, "Please wait...", Toast.LENGTH_LONG).show();
      	    	}
            	  
              }

         });
    }
    
    public static void saveGroup(Group group) {
    	Map<String, Group> groups = ((MyStorage)MyStorage.getAppContext()).getGroupsMap();
    	if (groups == null) {
    		groups = new HashMap<String, Group>();
    	}
    	
    	groups.put(group.getId(), group);

    	((MyStorage)MyStorage.getAppContext()).setGroupsMap(groups);
    }

    public void onClickCreateGroupButton(View view) {
		Intent intent = new Intent(GroupInterfaceActivity.this, CreateGroupActivity.class);
		startActivityForResult(intent, 1);
    }

    public void onClickSearchGroupButton(View view) {
		Intent intent = new Intent(GroupInterfaceActivity.this, FindGroupActivity.class);
		startActivityForResult(intent, 1);
    }

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      if(resultCode == RESULT_OK) {
    	  initFields();
    	  initCallbacks();
      }
	}
    
    public static void saveMessage(Message message) {
    	Map<String, Map<String, Message>> messages = ((MyStorage)MyStorage.getAppContext()).getMessagesMap();
    	Map<String, ArrayList<String>> messageIds = ((MyStorage)MyStorage.getAppContext()).getMessagesOrderedList();
    	if (messages == null) {
    		messages = new HashMap<String, Map<String, Message>>();
    	}
    	if (messageIds == null) {
    		messageIds = new HashMap<String, ArrayList<String>>();
    	}
    	
    	Map<String, Message> courseMessages = messages.get(groupId);
    	if (courseMessages == null) {
    		courseMessages = new HashMap<String, Message>();
    	}
    	
    	ArrayList<String> groupMessageIds = messageIds.get(groupId);
    	if (groupMessageIds == null) {
    		groupMessageIds = new ArrayList<String>();
    	}
    	
    	courseMessages.put(message.getId(), message);
    	groupMessageIds.add(message.getId());
    	messages.put(groupId, courseMessages);
    	messageIds.put(groupId, groupMessageIds);

    	Log.v("I am in", "messages");
    	((MyStorage)MyStorage.getAppContext()).setMessagesMap(messages);
    	((MyStorage)MyStorage.getAppContext()).setMessagesOrderedList(messageIds);
    }
	
	public static void saveData(String json_encoded) throws JSONException {
	    JSONObject json = new JSONObject(json_encoded);
		JSONArray jusers = null;
		JSONArray jmessages = null;
		
		if (json.has("users") && !json.isNull("users"))
			jusers = json.getJSONArray("users");
		else throw new JSONException ("Cannot decode users");
		Log.v("jusers", json.getJSONArray("users").toString());

		if (json.has("messages") && !json.isNull("messages"))
			jmessages = json.getJSONArray("messages");
		else throw new JSONException ("Cannot decode messages");
		Log.v("jmessages", json.getJSONArray("messages").toString());

		Map<String, User> usersMap = new HashMap<String, User>();
		ArrayList<User> users = ((MyStorage)MyStorage.getAppContext()).getUsersByJson(jusers);
		for (User user : users) {
			usersMap.put(user.getId(), user);
		}
		
		Group group = ((MyStorage)MyStorage.getAppContext()).getGroupsMap().get(groupId);
		ArrayList<Message> messages = ((MyStorage)MyStorage.getAppContext()).getMessagesByJson(jmessages, group, usersMap);
		
		for (Message message : messages) {
			Log.v("message text", message.getText());
			Log.v("message author name", message.getUser().getFullname());
			saveMessage(message);
		}
    }
	
	public static void getMessagesCallback(String notify) {
    	listView.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				// static content will be called
				//CreateNoteActivity.course = course;
				Log.v("enc", json_encoded);
				saveData(json_encoded);
				
              	Intent intent = new Intent(context, ChatGroupActivity.class);
              	intent.putExtra("groupId", groupId);
              	activity.startActivityForResult(intent, 1);
			} catch (JSONException e) {
				Log.v("exc", e.getMessage());
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
			}
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
