package fi.jamk.coolcalendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.util.DBService;

public class CourseFromServiceActivity extends BaseActivity {

	private TextView tUserMsg;
	private Button bSure;
	private static Button bDont;
	
	private String userId = null;
	private String courseId = null;
	private String name = null;
	private int diff = 0;
	
	private static Context context;
	private static Activity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eventservice);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	bSure = (Button) findViewById(R.id.bSure);
    	bDont = (Button) findViewById(R.id.bDont);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			userId = extras.getString("userId");
			courseId = extras.getString("courseId");
			name = extras.getString("name");
			diff = extras.getInt("diff");
		}
		
		tUserMsg.setText(Html.fromHtml("<div align=\"center\">You will have<br /><h2>" + name + "</h2>in " + diff + " minutes</div>"));
    }
    
    private void initCallbacks() {
    }
    
    public void onClickOkButton(View view) {
    	finish();
    }

    public void onClickCancelButton(View view) {
    	String retString = "";

    	bDont.setEnabled(false);
    	retString =
		    	DBService.updateServiceCourse(courseId, "off");

    	if (retString != "") {
    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
    	} else {
    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
    	}

    	updateEvent();
    }
    
    // update json in shared preferences
    private void updateEvent() {
		Context context = this.getApplicationContext();
		SharedPreferences sharedPref = this.getSharedPreferences("shared", Context.MODE_PRIVATE);
		String coursesJson = sharedPref.getString("events", "{ users:  { } }");

		String jsonToSave = coursesJson;
		try {
			JSONObject json = new JSONObject(coursesJson);
			JSONObject jsonUsers = json.getJSONObject("users");
	        JSONObject jsonUserData = jsonUsers.getJSONObject(userId);
			JSONArray jCourses = jsonUserData.getJSONArray("courses");
			
			for (int i = 0; i < jCourses.length(); i++) {
				JSONObject jCourse = jCourses.getJSONObject(i);
				
				if (jCourse.getString("id").equals(courseId)) {
					jCourses.getJSONObject(i).put("countdown", false);
					
					break;
				}
			}
			
			jsonUserData.put("courses", jCourses);
			jsonUsers.put(userId, jsonUserData);
			json.put("users", jsonUsers);
			
			jsonToSave = json.toString();
    	} catch (JSONException e) {
    		Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
    	}
		Log.v("save json as", jsonToSave);
		
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("events", jsonToSave);
		editor.commit();
    }

    public static void httpCallback(String notify) {
		bDont.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {
			activity.finish();
			Toast.makeText(context, "Course updated.", Toast.LENGTH_LONG).show();
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

}
