package fi.jamk.coolcalendar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.entity.Event;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.MyEventInterfaceAdapter;
import fi.jamk.coolcalendar.util.MyStorage;

public class EventInterfaceActivity extends BaseActivity {

	private String courseId;
	public static Course course = null;

	private TextView tUserMsg;
	private ListView listView;
	
	public static Context context;
	public static Activity activity;

	User user;

	// ids from events map will be stored here so we can get id by listview position
	private List<String> eventIds;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_eventinterface);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
    	user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
    	// get course
    	Map<String, Course> courseMap = ((MyStorage)MyStorage.getAppContext()).getCourseMap();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			courseId = extras.getString("courseId");
			course = courseMap.get(courseId);
		}
		
		// this shouldn't happen
		if (course == null) {
			finish();
		}

    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	listView = (ListView) findViewById(R.id.eventList);
    	
    	tUserMsg.setText("Events of " + course.getName());
    	
    	// get events
    	eventIds = new ArrayList<String>();
    	ArrayList<String> values = new ArrayList<String>();
    	Map<String, Map<String, Event>> events = ((MyStorage)MyStorage.getAppContext()).getEventsMap();
    	Map<String, Event> courseEvents = null;
    	if (events != null) {
	    	courseEvents = events.get(course.getId());
	    	
	    	if (courseEvents != null) {
		    	Iterator it = courseEvents.entrySet().iterator();
		        while (it.hasNext()) {
		            Map.Entry pairs = (Map.Entry)it.next();
		            eventIds.add((String)pairs.getKey());
		            Event event = (Event)pairs.getValue();
		            values.add(event.getDescription().toString());
		        }
	    	}
    	}

    	//ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, 
    		//	 android.R.layout.simple_list_item_1, android.R.id.text1, values);
    	MyEventInterfaceAdapter adapter = new MyEventInterfaceAdapter(context, values, activity, eventIds);
   		listView.setAdapter(adapter);
    }
    
    private void initCallbacks() {

    	
    	// ListView Item Click Listener
        listView.setOnItemClickListener(new OnItemClickListener() {

              public void onItemClick(AdapterView<?> parent, View view,
                 int position, long id) {
                
            	  Intent intent = new Intent(EventInterfaceActivity.this, DetailEventActivity.class);
            	  intent.putExtra("courseId", courseId);
            	  intent.putExtra("eventId", eventIds.get(position));
            	  startActivityForResult(intent, 1);
              }

         });
    }
    
    public static void saveEvent(Event event) {
    	Map<String, Map<String, Event>> events = ((MyStorage)MyStorage.getAppContext()).getEventsMap();
    	if (events == null) {
    		events = new HashMap<String, Map<String, Event>>();
    	}
    	
    	Map<String, Event> courseEvents = events.get(course.getId());
    	if (courseEvents == null) {
    		courseEvents = new HashMap<String, Event>();
    	}
    	
    	courseEvents.put(event.getId(), event);
    	events.put(course.getId(), courseEvents);
    	Log.v("I am in", "events");
    	((MyStorage)MyStorage.getAppContext()).setEventsMap(events);
    	((MyStorage)MyStorage.getAppContext()).saveEventsAndCoursesPreferences();
    }

    public void onClickCreateEventButton(View view) {
		Intent intent = new Intent(EventInterfaceActivity.this, CreateEventActivity.class);
		intent.putExtra("courseId", courseId);
		startActivityForResult(intent, 1);
    }

    public static void deleteFromEvent(String eventId) {
    	Map<String, Map<String, Event>> events = ((MyStorage)MyStorage.getAppContext()).getEventsMap();
    	
    	if (events != null) {
    		Map<String, Event> courseEvents = events.get(course.getId());
    		if (courseEvents != null) {
    			courseEvents.remove(eventId);
    		}
    		events.put(course.getId(), courseEvents);
    	}
    	
		((MyStorage)MyStorage.getAppContext()).setEventsMap(events);
    	((MyStorage)MyStorage.getAppContext()).saveEventsAndCoursesPreferences();
    }
    
    public static void deletedCallback(String notify) {
		if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String eventId = notify.substring(8).trim();
			
			activity.finish();
			activity.startActivity(activity.getIntent());
			deleteFromEvent(eventId);
			Toast.makeText(context, "Event deleted.", Toast.LENGTH_LONG).show();
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }
    
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      if(resultCode == RESULT_OK) {
    	  initFields();
    	  initCallbacks();
      }
	}

}
