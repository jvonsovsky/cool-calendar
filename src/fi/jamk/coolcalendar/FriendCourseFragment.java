package fi.jamk.coolcalendar;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import fi.jamk.coolcalendar.entity.Course;
import fi.jamk.coolcalendar.util.MyFriendCoursesAdapter;
import fi.jamk.coolcalendar.util.MyStorage;

public class FriendCourseFragment extends DialogFragment {
    static String userId;
    private List<Course> courses;
    private ArrayList<String> values;

	public static Activity activity;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    static FriendCourseFragment newInstance(String userId) {
    	FriendCourseFragment f = new FriendCourseFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("userId", userId);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.userId = getArguments().getString("userId");

		activity = getActivity();

        //Toast.makeText(this, json_encoded, Toast.LENGTH_LONG).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        getDialog().setTitle("Search results");

    	View v = inflater.inflate(R.layout.fragment_friendcourse, container, false);
        TextView tv1 = (TextView)v.findViewById(R.id.textView1);
        ListView listView = (ListView)v.findViewById(R.id.listView);
        
        Map<String, ArrayList<Course>> courseMap = ((MyStorage)MyStorage.getAppContext()).getUserCoursesMap();
        values = new ArrayList<String>();
        if (courseMap != null && courseMap.get(userId) != null) {
	        ArrayList<Course> courses = courseMap.get(userId);
	        for (Course c : courses) {
	        	values.add(c.getName());
	        }

	        MyFriendCoursesAdapter adapter = new MyFriendCoursesAdapter(activity, values, activity, courses, userId);
	        listView.setAdapter(adapter);
        }
        
        return v;
    }

}
