package fi.jamk.coolcalendar;

import java.util.Calendar;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import fi.jamk.coolcalendar.entity.Group;
import fi.jamk.coolcalendar.entity.User;
import fi.jamk.coolcalendar.util.DBService;
import fi.jamk.coolcalendar.util.MyStorage;

public class FindGroupActivity extends BaseActivity {

	private TextView tUserMsg;
	private EditText fName;
	private EditText fTeacher;
	private Spinner fDay;
	private TimePicker fStart;
	private TimePicker fEnd;
	public static Button fSubmit;
	
	public static Context context;
	public static Activity activity;
	
	public static User user;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_findgroup);

		context = this;
		activity = this;
		
		initFields();
		initCallbacks();
	}


    private void initFields() {
		user = ((MyStorage)MyStorage.getAppContext()).getUser();
    	
    	tUserMsg = (TextView) findViewById(R.id.tUserMsg);
    	fName = (EditText) findViewById(R.id.fName);
    	fTeacher = (EditText) findViewById(R.id.fTeacher);
    	fDay = (Spinner) findViewById(R.id.fDay);
    	fStart = (TimePicker) findViewById(R.id.fStart);
    	fEnd = (TimePicker) findViewById(R.id.fEnd);
    	fSubmit = (Button) findViewById(R.id.fSubmit);

    	// create days spinner
    	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
    	        R.array.days_array_with_none, android.R.layout.simple_spinner_item);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	fDay.setAdapter(adapter); 
    	
    	String timeZone = getString(R.string.time_zone);
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone( timeZone ));
    	fStart.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
    	fEnd.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
    	
    	tUserMsg.setText("Look for groups");
    }
    
    private void initCallbacks() {

    	fSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
    	
    	fName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					fTeacher.setText("");
					fDay.setSelection(0);
				}
			}
		});

    	fTeacher.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					fName.setText("");
					fDay.setSelection(0);
				}
			}
		});

    	fDay.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					fName.setText("");
					fTeacher.setText("");
				}
			}
		});

    }
    
    public static void saveData(String json_encoded) throws JSONException {
	    JSONObject json = new JSONObject(json_encoded);
		JSONObject jgroup = null;
		
		if (json.has("group") && !json.isNull("group"))
			jgroup = json.getJSONObject("group");

		Log.v("savegroup", json_encoded);
		try {
			if (jgroup != null)
				((MyStorage)MyStorage.getAppContext()).saveGroupByJson(user, jgroup);
		} catch (JSONException e) {
			Log.v("error", e.getMessage());
		}
    }

    private boolean validateFields() {
    	boolean valid = true;
    	
    	if (fName.getText().toString().length() == 0 && fTeacher.getText().toString().length() == 0 &&
    			fDay.getSelectedItemPosition() == 0) {
    		fName.setError("Neither name, teacher or date is set to search for.");
    		valid = false;
    	}
    	
    	return valid;
    }
    
    private void submit() {
    	if (validateFields()) {
	    	String retString = "";
	    	String dayStr = fDay.getSelectedItemPosition() == 0 ? "" : fDay.getSelectedItem().toString();
	
	    	fSubmit.setEnabled(false);
	    	retString =
		    	DBService.findGroup(fName.getText().toString(), fTeacher.getText().toString(),
		    			dayStr, fStart.getCurrentHour() + ":" + fStart.getCurrentMinute(),
		    			fEnd.getCurrentHour() + ":" + fEnd.getCurrentMinute());
	
	
	    	if (retString != "") {
	    		Toast.makeText(this, retString, Toast.LENGTH_LONG).show();
	    	} else {
	    		Toast.makeText(this, "Please wait...", Toast.LENGTH_LONG).show();
	    	}
    	}
    }
    
    public static void showDialog(String json_encoded) {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
        Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        DialogFragment newFragment = GroupSearchResultFragment.newInstance(json_encoded);
        newFragment.show(ft, "dialog");
    }
    
    public static void httpCallback(String notify) {
		fSubmit.setEnabled(true);
    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			showDialog(json_encoded);
			//activity.setResult(RESULT_OK, null);
			//activity.finish();
    	} else {
    		// something wrong
    		Toast.makeText(context, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }

	// duplicate from MyStorage, no time for refactor
    public static Group saveGroupByJson(User user, JSONObject json) throws JSONException {
    	String id = "";
		String name = "";
		String teacher = "";
		String day = "";
		String start = "";
		String end = "";
		
		if (json.has("_id"))
			id = json.getString("_id");
		if (json.has("name"))
			name = json.getString("name");
		if (json.has("teacher"))
			teacher = json.getString("teacher");
		if (json.has("day"))
			day = json.getString("day");
		if (json.has("start"))
			start = json.getString("start");
		if (json.has("end"))
			start = json.getString("end");
		
		Group group = new Group(id, user, name, teacher, day, start, end);
		return group;
    }

    public static void enterGroupCallback(String notify) {
    	User user = ((MyStorage)MyStorage.getAppContext()).getUser();
		fSubmit.setEnabled(true);

    	if (notify.length() > 8 && notify.substring(0, 7).equals("success")) {

			String json_encoded = notify.substring(8);
			
			try {
				JSONObject json = new JSONObject(json_encoded);
				JSONObject jGroup = json.getJSONObject("group");
				
				Group group = saveGroupByJson(user, jGroup);
				GroupInterfaceActivity.saveGroup(group);
			} catch (JSONException e) {
				Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();
			}

			activity.setResult(RESULT_OK, null);
			activity.finish();
    	} else {
    		// something wrong
    		Toast.makeText(activity, notify, Toast.LENGTH_LONG).show();
    	}
    	
    }
}
